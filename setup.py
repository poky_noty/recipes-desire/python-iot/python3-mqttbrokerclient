from setuptools import setup

install_requires = [
    "paho-mqtt==1.4.0",
    "sqlalchemy==1.1.17",
    "pymysql==0.9.3",
    "psutil==5.6.3",
    #"dmidecode==3.2",
    #"asyncio>=0.1",
    "apscheduler==3.6.3",
    "redis==2.10.6"
]

setup(
    name='mqttbrokerclient',
    version='2.8.4',
    description=
    'A client that connects with an MQTT-Broker and exchanges IoT-messages',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url='https://gitlab.com/recipes-desire/python-iot/python3-mqttbrokerclient',
    license='MIT license',
    packages=['mqttbrokerclient', 
              'mqttbrokerclient.datastore',
              'mqttbrokerclient.datastore.influxdb',
              'mqttbrokerclient.jobs', 'mqttbrokerclient.jobs.central_server', 'mqttbrokerclient.jobs.edge_board',
              'mqttbrokerclient.mechanisms', 'mqttbrokerclient.mechanisms.linkages', 'mqttbrokerclient.mechanisms.peasants', 'mqttbrokerclient.mechanisms.warriors',
              'mqttbrokerclient.mosquitto', 'mqttbrokerclient.mosquitto.messages', 'mqttbrokerclient.mosquitto.messages.iot', 
              'mqttbrokerclient.mosquitto.messages.monitoring',
              'mqttbrokerclient.utils'],
    #package_data=[],
    entry_points={
        'console_scripts': [
            #'mqttbrokerclient_subscriber=mqttbrokerclient.simple_message_manager:main_subscriber',
            'mqttbrokerclient-executor=mqttbrokerclient.__main__:main'
        ]
    },
    install_requires=install_requires,
    #extras_require={},
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.4',
        'Topic :: Messaging :: IoT :: Monitoring :: Broker :: Mosquitto :: Python-Client :: Paho',
    ],
    zip_safe=True)

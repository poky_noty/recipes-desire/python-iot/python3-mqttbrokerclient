import paho.mqtt.subscribe as subscribe

#import logging
#logging.basicConfig(level=logging.DEBUG)
#logger = logging.getLogger(__name__)


def topics_receive_and_acknowledge(logger,
                                   topics="monitoring/hosts/up",
                                   hostname="broker-2-service",
                                   retained=False,
                                   msg_count=2):
    m_essages = subscribe.simple([topics], hostname, retained, msg_count)

    for iot_msg in m_essages:
        # This is the response to a Server-Ping
        # db_manager.save_ping_response_for_server(iot_msg)

        logger.debug(iot_msg.topic)
        logger.debug(iot_msg.payload)

def quick_subscribe_test():
    pass

"""
Here you can find the code for scheduler that handles important jobs at ...
CENTRAL-SERVER side.
"""

import os
import asyncio
import logging

import traceback

from apscheduler.schedulers.asyncio import AsyncIOScheduler

from .cs_jobs import CSJobs
from mqttbrokerclient.mosquitto.client_subscriber import ClientSubscriber

ERROR_CODE_SCHEDULE_ALL=100

class CentralServerSchedulerException(Exception):
    """
    Dedicated exception for anomalies during the edge-board scheduler ...
    operation with a descriptive message that describes the failure cause.
    """

    def __init__(self, message, error_code):
        self.error_code = error_code

        super().__init__(message)


class CSJobScheduler(object):
    """
    Schedules the tasks that we want to run concurrently in the edge-board
    """

    def __init__(self):
        "The scheduler for edge-board jobs --> Using the APScheduler library"
        self._scheduler = AsyncIOScheduler()

        self._jobs = CSJobs()

        "Polling Freqiencies"
        self._database_polling_freq_secs = int(
            os.environ["DATABASE-POLLING-FREQ-SECS"])

        self._logger = logging.getLogger("mqttbroker-client")
        self._logger.debug("Central-Server MQTT-Client --> Scheduler Ready !")

    def schedule_all_jobs(self):
        """
        Schedule all jobs for CENTRAL-SERVER data handling mechanism ...
        to start running concurrently
        """
        try:
            """
            Handle all jobs ... Database + MQTT-Subscriber
            """
            self._scheduler.add_job(
                self._jobs.query_database_for_new, 'interval',
                seconds=self._database_polling_freq_secs)

            #self._scheduler.add_job(
            #    self._jobs.collect_network_monitoring_data, 'interval',
            #    seconds=self._network_data_freq_secs)

            self._scheduler.start()

            self._logger.debug("Press Ctrl+C")

            try:
                #loop = asyncio.get_event_loop()
                #loop.run_forever(ClientSubscriber(loop).main())
                #loop.close()
                loop = asyncio.get_event_loop()
                loop.create_task(ClientSubscriber(loop, is_sensordata=True).main())

                loop.run_forever()
                #loop.close()
            except (KeyboardInterrupt, SystemExit):
                pass

        except Exception as my_ex:

            self._logger.error(
                "Scheduling All Edge Board Jobs Raised an Exception --> "
                f"{str(my_ex)} !\n")

            self._logger.error(traceback.print_exception(type(my_ex), my_ex, my_ex.__traceback__))

            #raise CentralServerSchedulerException("", ERROR_CODE_SCHEDULE_ALL)

    def schedule_only_perform_jobs(self):
        """
        Schedule the data collection mechanism for polling the underlying ...
        platform for its operational status.
        """
        #self._jobs._collect_performance_data()
        pass

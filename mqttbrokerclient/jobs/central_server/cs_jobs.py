import logging

from mqttbrokerclient.datastore.new_records_discovery \
    import NewRecordsDiscovery

class CSJobs(object):
    """
    This class knows all Jobs that are associated with edge-board.
    What kind of data the monitoring system wants ?
    This class can call the appropriate data collectors.
    """

    def __init__(self):
        self._logger = logging.getLogger("mqttbroker-client")
        self._logger.debug(
            "Administrator Knows --> Jobs for Edge Board and "
            "Sensor/ Network/ System/ Power performance")

    def query_database_for_new(self):
        """
        Data Collected --> New records at database that are not yet handled
        """
        pass
        #self._logger.debug(
        #    "Ready to Query Database for New Records that Need Handling !")

        #job = SystemPerformanceJob()
        #job.collect_resources_used()
        #data_collected = job.performance_data

        #self._logger.debug(f"Data Collected ... {data_collected}")

        """
        ****************************************************
        ****************************************************
        Publish Collected Data --> Send to Local MQTT-Broker
        ****************************************************
        ****************************************************
        """
        #simple_publisher = ClientPublisher()
        #simple_publisher.connect_to_broker()
        #simple_publisher.publish_and_wait(data_collected)

        #send_message_about_board_performance(data_collected)

        #discovery = NewRecordsDiscovery()
        #new_records = discovery.find_all_new()

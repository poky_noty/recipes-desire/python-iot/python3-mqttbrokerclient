"""
This module contains the necessary classes for high-level edge-board ...
job management.
Schedule All/ Many/ Few/ None of jobs.
"""

import logging

from mqttbrokerclient.jobs.edge_board.system_performance_job \
    import SystemPerformanceJob
from mqttbrokerclient.mechanisms.linkages.simple_mqtt_client_support \
    import send_message_about_board_performance

"""
*************************************
MQTT-Simple-Publisher: Connect & Wait
*************************************
"""
from mqttbrokerclient.mosquitto.client_publisher import ClientPublisher

class EBJobs(object):
    """
    This class knows all Jobs that are associated with edge-board.
    What kind of data the monitoring system wants ?
    This class can call the appropriate data collectors.
    """

    def __init__(self):
        self._logger = logging.getLogger("mqttbroker-client")
        self._logger.debug(
            "Administrator Knows --> Jobs for Edge Board and "
            "Sensor/ Network/ System/ Power performance")

    def collect_performance_data(self):
        """
        Data Collected --> System Performance CPU/ RAM/ HD/ ...
        """
        self._logger.debug("Ready to Collect Performance Data !")

        job = SystemPerformanceJob()
        data_collected = job.collect_resources_used()
        
        self._logger.debug(f"Data Collected ... {data_collected}")

        """
        ****************************************************
        ****************************************************
        Publish Collected Data --> Send to Local MQTT-Broker
        ****************************************************
        ****************************************************
        """
        #simple_publisher = ClientPublisher()
        #simple_publisher.connect_to_broker()
        #simple_publisher.publish_and_wait(data_collected)

        send_message_about_board_performance(data_collected)

    def collect_sensor_data(self):
        self._logger.debug("Ready to Collect Sensor Measured Data !")
        pass

    def collect_network_monitoring_data(self):
        self._logger.debug("Ready to Collect Network Monitoring Data !")
        pass

    def collect_power_consumption(self):
        self._logger.debug("Ready to Collect Power-Consumption Data !")
        pass


class EdgeBoardPerformnanceData(object):
    def __init__(self):
        pass

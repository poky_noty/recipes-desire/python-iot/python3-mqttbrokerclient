"""
This module contains the code for storing the aquired system performance ...
data of underlying platform, like the CPU/ Memory/ HD reserved.
"""

class SystemPerformanceData(object):
    """
    Class with atributes that store data returned by query for ...
    system resources used
    """

    def __init__(self):
        self._cpu_stats = None
        self._disk_stats = None
        self._process_stats = None

    def _get_all_stats(self):
        """
        Returns a structure containing all measured data related with ...
        system performance.
        An aggregation of cpu + memory + disk + processes statistics.
        """
        all_stats = {}

        all_stats["cpu-stats"] = self._cpu_stats
        all_stats["disk-stats"] = self._disk_stats
        all_stats["process-stats"] = self._process_stats

        return all_stats

    def set_cpu_stats(self, cpu_stats):
        """
        Assign the measured values to class attributes (with descriptive names)
        """
        self._cpu_stats = cpu_stats

    def set_disk_stats(self, disk_stats):
        """
        Assigns latest disk-statistics collected to a global structure for
        system performance data.
        """
        self._disk_stats = disk_stats

    def set_process_stats(self, process_stats):
        """
        Assign latest process-statistics collected to a global structure for
        system performance data.
        """
        self._process_stats = process_stats

    def __str__(self):
        """
        Returns a string containing all performance-data data elements
        """
        all_stats = self._get_all_stats()

        if all_stats is None:
            msg = "Problem with All_Stats ... Value is None"
        else:
            msg = f"All_Stats are Fine ... Value = {str(self._cpu_stats)})"

        #return str(self._cpu_stats)
        return msg

import logging

class ProcessStats(object):
    def __init__(self):
        self._logger = logging.getLogger("mqttbroker-client")

    def collect_running_info(self):
        self._logger.debug("+++++++++++++++++++++++")

        return -1

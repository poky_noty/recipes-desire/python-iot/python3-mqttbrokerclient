"""
Module for collecting CPU-Statististics of underlying system.
"""

import psutil
import logging


class CPUStats(object):
    """
    Aware of current system-wide cpu utilisation.
    """

    def __init__(self):
        self._cpu_stats_all = {}

        self._logger = logging.getLogger("mqttbroker-client")
        self._logger.debug("Inside the CPUStats manager !")

    def collect_current_cpu_stats(self):
        """
        Ask the underlying platform for current cpu-utilization statistics
        """

        # (1) cpu used
        cpu_percent = psutil.cpu_percent(interval=1)
        self._logger.debug(f"CPU-Percent = {cpu_percent}")
        self._cpu_stats_all["cpu-percent"] = cpu_percent

        # (2) cpu times
        cpu_times = psutil.cpu_times(percpu=False)
        self._logger.debug(f"CPU-Times={cpu_times}")

        self._cpu_stats_all["cpu-times"] = cpu_times

        return self._cpu_stats_all

    def __str__(self):
        """
        Stringnify the class returning a long text containing all important
        statistics fro CPU, Disk, ....
        """
        return \
            "CPU Statistics of Edge Board:cpu-percent=" + \
            str(self._cpu_stats_all["cpu-percent"])

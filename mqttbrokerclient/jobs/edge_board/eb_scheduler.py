"""
Here you can find the code of scheduler that posts important jobs at ...
edge board.
"""

import os
import asyncio
import logging

from apscheduler.schedulers.asyncio import AsyncIOScheduler

from .eb_jobs import EBJobs

ERROR_CODE_SCHEDULE_ALL=100


class EdgeBoardSchedulerException(Exception):
    """
    Dedicated exception for anomalies during the edge-board scheduler ...
    operation with a descriptive message that describes the failure cause.
    """

    def __init__(self, message, error_code):
        self.error_code = error_code

        super().__init__(message)


class EBJobScheduler(object):
    """
    Schedules the tasks that we want to run concurrently in the edge-board
    """

    def __init__(self):
        "The scheduler for edge-board jobs --> Using the APScheduler library"
        self._scheduler = AsyncIOScheduler()

        self._jobs = EBJobs()

        "Polling Freqiencies"
        self._system_performance_freq_secs = int(
            os.environ["SYSTEM-PERFORMANCE-FREQ-SECS"])
        self._sensor_data_freq_secs = int(
            os.environ["SENSOR-DATA-FREQ-SECS"])
        self._network_data_freq_secs = int(
            os.environ["NETWORK-DATA-FREQ-SECS"])
        self._power_consumption_freq_secs = int(
            os.environ["POWER-CONSUMPTION-FREQ-SECS"])

        self._logger = logging.getLogger("mqttbroker-client")
        self._logger.debug("Edge-Board MQTT-Client --> Scheduler Ready !")

    def schedule_all_jobs(self):
        """
        Schedule all jobs for edge-board data collection mechanism ...
        to start running concurrently
        """
        try:
            """
            Handle all jobs ... Net + Operation + IoT
            """
            self._scheduler.add_job(
                self._jobs.collect_sensor_data, 'interval',
                seconds=self._sensor_data_freq_secs)

            self._scheduler.add_job(
                self._jobs.collect_network_monitoring_data, 'interval',
                seconds=self._network_data_freq_secs)

            self._scheduler.add_job(
                self._jobs.collect_power_consumption, 'interval',
                seconds=self._power_consumption_freq_secs)

            self._scheduler.add_job(
                self._jobs.collect_performance_data, 'interval',
                seconds=self._system_performance_freq_secs)

            self._scheduler.start()

            self._logger.debug("Press Ctrl+C")

            try:
                asyncio.get_event_loop().run_forever()
            except (KeyboardInterrupt, SystemExit):
                pass

        except Exception as my_ex:
            self._logger.error(
                "Scheduling All Edge Board Jobs Raised an Exception"
                f"{str(my_ex)} !")

            raise EdgeBoardSchedulerException("", ERROR_CODE_SCHEDULE_ALL)

    def schedule_only_perform_jobs(self):
        """
        Schedule the data collection mechanism for polling the underlying ...
        platform for its operational status.
        """
        self._jobs.collect_performance_data()

"""
A modules that defines jobs for collecting data from monitored netwroks ...
like, which Servers/ VMs and Containers are up and running.
"""

import logging


class NetworkMonitoringJob(object):
    """
    This class collects performance data about monitored subnets.
    There is a mechanism that uses the nmap tool and discovers running ...
    servers and open ports.
    The findings are stored at Redis and this class is polling Redis for new
    records.
    """

    def __init__(self):
        self._redis_records = []

        self._logger = logging.getLogger("mqttbroker-client")

    def poll_redis(self):
        """
        Poll redis for new records stored from nmap tool manager.
        """
        pass

    @property
    def redis_networks(self):
        """
        Returns found records after redis polling.
        """
        return self._redis_records

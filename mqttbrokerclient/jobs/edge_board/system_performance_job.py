"""
A modules that defines needed jobs for collecting data for ...
underlying platforms, like CPU/ Memord/ Hard-Disk usage.
"""

import logging

from mqttbrokerclient.jobs.edge_board.cpu_statistics import CPUStats
from mqttbrokerclient.jobs.edge_board.disk_statistics import DiskStats
from mqttbrokerclient.jobs.edge_board.process_statistics import ProcessStats
from mqttbrokerclient.jobs.edge_board.system_performance_data import SystemPerformanceData

class SystemPerformanceJob(object):
    """
    This class collects performance data for underlying platform ...
    (the edge-board linuxized processor).
    There is a mechanism that uses the psutil tool for collecting resources ...
    consumption data.
    """

    def __init__(self):
        self._performance_data = SystemPerformanceData()

        self._logger = logging.getLogger("mqttbroker-client")
        self._logger.debug("Inside Data Collection Job --> "
                           "System Performance !")

    def collect_resources_used(self):
        """
        Collects current system performance ... get-cpu-usage and other
        """

        # CPU
        cpu_stats = self._collect_cpu_stats()
        self._performance_data.set_cpu_stats(cpu_stats)

        # Hard Disk
        disk_stats = self._collect_disk_stats()
        self._performance_data.set_disk_stats(disk_stats)

        # Processes
        process_stats = self._collect_process_stats()
        self._performance_data.set_process_stats(process_stats)

        # Memory

        return self._performance_data

    def _collect_cpu_stats(self):
        """
        Collect various cpu statistics.
        """
        cpu = CPUStats()
        cpu_stats = cpu.collect_current_cpu_stats()

        self._logger.debug(
            "&&&&&&& COLLECT CPU-STATS --> %s",
            cpu_stats["cpu-percent"])

        #return cpu_stats
        return str(cpu)

    def _collect_disk_stats(self):
        """
        Collect various hard-disk statistics.
        """
        disk = DiskStats()
        current = disk.collect_space_reserved()

        return current

    def _collect_process_stats(self):
        process = ProcessStats()
        current = process.collect_running_info()

        return current

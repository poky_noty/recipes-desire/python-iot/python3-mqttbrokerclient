import logging

class DiskStats(object):
    def __init__(self):
        self._logger = logging.getLogger("mqttbroker-client")

    def collect_space_reserved(self):
        self._logger.debug("****************************")

        return -1

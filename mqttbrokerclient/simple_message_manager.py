import logging
import asyncio

from .simple_message_publisher import create_topic_publish_message
from .simple_message_subscriber import topics_receive_and_acknowledge

from .mosquitto.client_subscriber import ClientSubscriber
from .mosquitto.client_publisher import ClientPublisher

def prepare_mqtt_clients():
    """
    Need a client to contact broker and create a Publisher and Subscriber.
    """
    logger = logging.getLogger("mqttbroker-client")

    logger.debug("Inside MQTT-Message Manager")

    logger.debug("Ready to Subscribe to ADMINISTRATION Topic")
    subscribe_admin_topic()

    logger.debug("Ready to Publish to ADMINISTRATION Topic")
    publish_admin_topic()

    logger.debug("Ready to Subscribe to SENSORDATA Topic")
    subscribe_sensordata_topic()

    logger.debug("Ready to Publish to SENSORDATA Topic")
    publish_sensordata_topic()


####
# Module Entrypoint ... Publisher and send a message for a Topic
def publish_admin_topic():
    """
    Responsible for creating a Client-Publisher to ADMINISTRATION topic
    """
    # Follow the PUBLISHER path ... send messages
    admin_publisher = ClientPublisher(is_admin=True)
    #admin_publisher.connect_to_broker()
    #admin_publisher.publish_and_wait()


####
# Module Entrypoint ... Subscriber consume messages on a Topic
def subscribe_admin_topic():
    """
    Responsible for creating an Client-Subscriber to ADMINSISTRATION topic.
    """
    # Follow the SUBSCRIBER path ... consume messages
    loop = asyncio.get_event_loop()
    #loop.run_forever()

    loop.run_until_complete(ClientSubscriber(loop=loop, is_admin=True).main())

    loop.close()

    #admin_subscriber = ClientSubscriber(is_admin=True)
    #admin_subscriber.connect_to_broker()
    #admin_subscriber.subscribe_to_topic()


def subscribe_sensordata_topic():
    #sensordata_subscriber = ClientSubscriber(is_sensordata=1)
    pass


def publish_sensordata_topic():
    """
    """
    #sensordata_publisher = ClientPublisher(is_sensordata=1)
    pass

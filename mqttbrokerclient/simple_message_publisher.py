import os

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

#from .mosquitto.connection import MqttConnection
#from .messages.monitoring.host_discovery_msg import publish_msg_hosts_up

import logging
#logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

def on_subscribe(mqttc, obj, mid, granted_qos):
    logger.debug("Subscribed: %s - %s", str(mid), str(granted_qos))

def on_publish(client, userdata, mid):
    logger.debug("MID: %s", str(mid))

def on_log(mqttc, obj, level, string):
    logger.debug(string)


def on_message(mqttc, obj, msg):
    logger.debug("%s - %s - %s", msg.topic, str(msg.qos), str(msg.pauload))

def on_connect(mqttc, obj, flags, rc):
    logger.debug("rc: %s", str(rc))


def create_topic_publish_message(logger):
    # Read Environment Variables
    host = os.environ["MOSQUITTO_HOST"]
    port = os.environ["MOSQUITTO_PORT"]
    port = int(port)

    #client = MqttConnection(logger, host, port)
    client = mqtt.Client("miltos")
    client.on_message = on_message
    client.on_connect = on_connect
    client.on_publish = on_publish
    client.on_subscribe = on_subscribe

    #client.connect_client_blocking()
    client.connect(host, port)

    client.loop_start()

    #hosts_array = ["host A", "host B", "host C", "host D"]
    #msgs = publish_msg_hosts_up(hosts_array)

    #client.publish.multiple(msgs)
    #client.publish.multiple([("monitoring/hosts/up", "12 degrees", 1, False),("monitoring/hosts/up", "4 humidity", 2, False)])

    # Publish Message #1
    logger.debug( "Starting Publishing Many Messages to MQTT-Broker" )

    client.publish("monitoring/hosts/up", "Miltos Run", qos=0)

    # Publish Message #2
    #infot = client.publish("monitoring/hosts/up", "Miltos Walk", qos=2)

    #infot.wait_for_publish()

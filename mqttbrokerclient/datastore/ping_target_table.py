from sqlalchemy import Table, Column, Integer, String, Boolean, DateTime, MetaData
from sqlalchemy.sql import select

from mqttbrokerclient.datastore.abstract_table import AbstractTable

from mqttbrokerclient.datastore.db_manager import DBManager
#from mqttbrokerclient.datastore.exter

class PingTargetTable(AbstractTable):
    """
    Code for transactions with TABLE --> Ping_Targets
    """

    def __init__(self):
        self.connection = DBManager().connection


        metadata = MetaData()
        self._table = Table(
            'external_servers', metadata,
            Column('id', Integer(), primary_key=True),
            Column('dns_name', String(255), index=True),
            Column('ip', String(55)),
            Column('mac_addr', String(255), unique=True),
            Column('is_interesting', Boolean),
            Column('is_up', Boolean),
            Column('last_observation_datetime', DateTime),
            Column('is_new', Boolean)
        )

    def check_for_new_records(self):

        self.connection.execute("select IP from target_servers WHERE is_new=True")
        
        selection = select([self._table])
        result_proxy = self.connection.execute( selection )
        results = result_proxy.fetchall()

        #external_servers_array = []
        #for result in results:
            #extServersDAO = ExternalServersTable( result )
            #external_servers_array.append( extServersDAO )

"""
A module with exceptions related to establishing a datastore connection.
"""


class DatastoreConnectionError(Exception):
    """
    A custom exception raised if connection failure.
    """

    def __init__(self, msg):
        super().__init__(
            self,
            f"Cannot establish connection with database ... {msg}")

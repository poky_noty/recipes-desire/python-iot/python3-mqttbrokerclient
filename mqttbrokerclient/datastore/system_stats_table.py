from sqlalchemy import Table, MetaData, Column, Integer, String, DateTime
from sqlalchemy.sql import select

from .abstract_table import AbstractTable

import logging

class SystemStatsTable(AbstractTable):
    #def __init__(self):
    #    self.logger = logging.getLogger("mqttbroker-client")
    #    self.logger.debug("Inside SystemStatsTable !")

    def create_table(self):
        metadata = MetaData()
        self._table = Table(
            "system_stats",
            metadata,
            Column('stats_id', Integer(), primary_key=True),
            Column('board_id', String(80)),
            Column('measurement', String(300)),
            Column('exact_time', DateTime)
            )

    def get_all_records(self):
        selection = select([self._table])

        result_proxy = self._connection.execute(selection)

        result = result_proxy.fetchall()

        self._logger.debug("Records Found in SysStats Table ...%d", len(result))
        #return result


    def check_for_new_records(self):
        raise Exception()

    def save_data(self, record):
        self._logger.debug(
          f"Ready to Save Data Object to System_Stats table .. {record}")

        ins = self._table.insert().values(
            board_id='icom-board-dummy',
            measurement=record.measurement
        )

        self._connection.execute(ins)

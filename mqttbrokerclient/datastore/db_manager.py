import logging
import inspect
import sys

from sqlalchemy import create_engine

from mqttbrokerclient.datastore.datastore_configuration \
    import DatastoreConfiguration

from mqttbrokerclient.datastore.datastore_configuration \
    import get_database_connection_params
from mqttbrokerclient.datastore.connection_error \
    import DatastoreConnectionError
from mqttbrokerclient.datastore.unsupported_database \
    import UnsupportedDatabase

from mqttbrokerclient.datastore.target_servers_table import TargetServersTable
from mqttbrokerclient.datastore.target_subnets_table import TargetSubnetsTable
from mqttbrokerclient.datastore.edge_boards_table import EdgeBoardsTable
from mqttbrokerclient.datastore.system_stats_table import SystemStatsTable
from mqttbrokerclient.datastore.system_stats_record import SystemStatsRecord
from mqttbrokerclient.datastore.hot_air_AC_temperature_table import HotAirACTemperatureTable
from mqttbrokerclient.datastore.hot_air_AC_temperature_record import HotAirACTemperatureRecord
from mqttbrokerclient.datastore.ac_power_consumption_record import ACPowerConsumptionRecord
from mqttbrokerclient.datastore.ac_power_consumption_table import ACPowerConsumptionTable

class DBManager(object):
    """
    Basic database management functionalities ...
    connect to database and query/ update.tables.
    """

    def __init__(self):
        db_conn_params = get_database_connection_params()

        self._host = db_conn_params.get("host")
        self._user = db_conn_params.get("user")
        self._password = db_conn_params.get("password")
        self._dbname = db_conn_params.get("dbname")

        # Are there any values assigned to important parameters?
        if self._host is None or not self._host:
            # Database URL not available
            raise Exception("Undefined Database Host .. Cannot proceed !")

        if self._user is None or not self._user:
            # Credentials Username not available
            raise Exception("Undefined Username ... Cannot proceed !")

        if self._password is None or not self._password:
            # Credentials Password not available
            raise Exception("Undefined Password ... Cannot proceed !")

        if self._dbname is None or not self._dbname:
            raise Exception("Undefined DB-Name ... Cannot proceed !")

        # Defne database-transactions Logger
        self._logger = logging.getLogger("mqttbroker-client")
        self._logger.debug("Inside Database Manager")

        # **********************************
        # ** Start the SQL-Alchemy Engine **
        # **********************************
        if DatastoreConfiguration().is_mysql():
            connection_str = "mysql+pymysql://" + self._user +\
                ":" + self._password
            connection_str += "@" + self._host + "/" + self._dbname
        else:
            # This database is NOT supported
            raise UnsupportedDatabase(
                "Do not support this database type ... "
                "Cannot prepare SQL statements !")

        self._logger.debug(
            "Connection string to db ... %s", connection_str)

        # SQLAlchemy engine knows the SQL-dialect for used datastore
        engine = create_engine(
            connection_str, pool_size=20, pool_recycle=3600, echo=False)

        self._connection = engine.connect()

    @property
    def user(self):
        """ Returns the user name used for db-connection """
        return self._user

    @property
    def password(self):
        """ Returns the password used for db-connection """
        return self._password

    @property
    def host(self):
        """ Returns the ip or dns-name of db-server """
        return self._host

    @property
    def dbname(self):
        """ Returns the name database that contains all interesting tables """
        return self._dbname

    @property
    def connection(self):
        """ Returns the esatblished connection with database """
        return self._connection

    def close_connection(self):
        """ Close connection after database transaction """
        self._connection.close()

    def connect_or_raise_exception(self):
        """
        Used for verifying the external-configuration-data provided ...
        In case of correct data a successful database connection is established
        """
        self._logger.debug(
            "Inside DBManager ... Ready to check connection : "
            "Sending Query to DUMMY_TABLE"
        )

        if self._connection is None:
            raise DatastoreConnectionError("Connection object is Null")
        else:
            # Query a DUMMY table and see if any records found
            target_servers = TargetServersTable(self._connection)
            rows = target_servers.get_all_records()

            for row in rows:
                self._logger.debug(f"Row in Dummy Table --> {str(row)}")

            self._logger.debug("Check for New Records")
            target_servers.check_for_new_records()

            self._logger.debug("Check for New Records in TARGET_SUBNETS")
            target_subnets = TargetSubnetsTable(self._connection)
            rows = target_subnets.get_all_records()
            for row in rows:
                self._logger.debug(f"{row}")

            self._logger.debug("Ready to Query the EDGE_BOARDS table")
            edge_boards = EdgeBoardsTable(self._connection)
            rows = edge_boards.get_all_records()

            for row in rows:
                self._logger.debug(f"Row in EDGE_BOARDS Table ...{row}")

    def get_new_subnets_records(self):
        """
        Check whether the system administrator added new local subnets for ...
        inspection through the GUI.
        """
        # simple query filter = is_new --> True
        for name, obj in inspect.getmembers(sys.modules[__name__], inspect.isclass):
            self._logger.debug(f"==> Name {name}")


        if name is TargetSubnetsTable.__class__:
                self.check_target_subnets_table()

        return -1

    def check_target_subnets_table(self):
        pass

    def save_system_stats(self, measurement, the_time):
        self._logger.debug("I am Database Manger ... Ready to Save Updated System Stats Records !")

        record = SystemStatsRecord(measurement=measurement, time_taken=the_time)

        table = SystemStatsTable(self._connection)
        table.create_table()
        table.save_data(record)
        table.get_all_records()

    def save_ac_hot_air_temperature(self, data):
        self._logger.debug("I am Database Manager ... Ready to Save New Hot-Air AC Temperature Measurement !")

        record = HotAirACTemperatureRecord(capture_time = data[0], measured_temperature = data[1]) 
        table = HotAirACTemperatureTable(self._connection)
        table.save_data(record) 

    def save_ac_power_consumption(self, data):
        record = ACPowerConsumptionRecord(capture_time=data[0], measured_consumption=data[1])
        table = ACPowerConsumptionTable(self._connection)
        table.save_data(record)

    #def __del__(self):
        #self._logger.debug(
        #    "!!! Nice Message to Print for Destructors "
        #    f"{inspect.getouterframes(inspect.currentframe(), 2)}!!!")

    #def start_engine(self):

        #self.check_and_create_db()

        #self.metadata = MetaData()

        # TABLE --> Ping_Targets
        #self.external_servers_tbl = Table( 'external_servers', self.metadata,
        #    Column('id', Integer(), primary_key=True),
        #    Column('dns_name', String(255), index=True),
        #    Column('ip', String(55)),
        #    Column('mac_addr', String(255), unique=True),
        #    Column('is_interesting', Boolean),
        #    Column('is_up', Boolean),
        #    Column('last_observation_datetime', DateTime)
        #)

        # TABLE --> Scan_Ports
        #self.open_ports_tbl = Table( 'open_ports', self.metadata,
        #    Column('port_id', Integer, primary_key=True),
        #    Column('server_id', Integer, ForeignKey('external_servers.id')),
        #    Column('port_num', Integer),
        #    Column('protocol', String(3), CheckConstraint("protocol='tcp' OR protocol='udp'")),
        #    Column('service_name', String(55)),
        #    Column('is_interesting', Boolean),
        #    Column('is_up', Boolean),
        #    Column('last_obesrvation_datetime', DateTime)
        #)

        # TABLE -> PING_PORTS_FOUND
        # self.ping_ports_found_tbl = PingPortScanTable( self.engine )

    """
    def connect(self):
        #Establish a database connection

        self.connection = self.engine.connect()


    def insert_record_to_table(self):
        return True


    def save_ping_response_for_server(self, message):
        table =  ServerAliveResponsesTable(self.connection)
        table.save_data(message)
    """

    def check_table_for_new_records(self,  table_name):
        # Query table and get only new records ....
        #     the column 'isNew' helps the query to select the correct records
        #table = PingTargetTable(self.connection)
        #table.check_for_new_records()
        return -1


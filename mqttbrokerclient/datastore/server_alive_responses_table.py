from sqlalchemy import Table, Column, Integer, Boolean, DateTime, MetaData

from mqttbrokerclient.datastore.abstract_table import AbstractTable

class ServerAliveResponsesTable(AbstractTable):
    def __init__(self):
        """
        id
        server_id
        is_up
        observation_time
        """
        pass

    def create_table(self):
        """
        Create the ServerResponsesTable in an empty database
        """
        metadata = MetaData()
        self._table = Table( 'server_alive_responses', metadata,
                Column('id', Integer(), primary_key=True),
                Column('server_id', foreign_key=True),
                Column('isAlive', Boolean),
                Column('last_observation_datetime', DateTime)
                )

    def save_data(self):
        """
        """
        pass

"""
Custom exceptions for backend datastore management.
"""


class UnknownDatastoreManager(Exception):
    """
    Custom exception that is raised when ...
    Application cannot recognize the datastory manager ...
    and doesn't know how to handle Save & Query data-requests
    """

    def __init__(self):
        super().__init__(self, "Do not know how to handle this kind of "
                               "Datastore")

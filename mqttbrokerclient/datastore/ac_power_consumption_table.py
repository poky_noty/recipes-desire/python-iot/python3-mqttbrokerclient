"""
This module is responsible for performing all database transactions with table .... ac-power-consumption
Monitoring application calls this class ... for record insert/ query and delete 
"""

from sqlalchemy import Table, MetaData, Column, Integer, String, Numeric, Float

from .abstract_table import AbstractTable

class ACPowerConsumptionTable(AbstractTable):
    def get_all_records(self):
        self._logger.warn("Not Yet Implemented !")

    def create_table(self):
        metadata = MetaData()

        self._table = Table(
            "sensor_ac_power_consumption",
            metadata, 
            Column("power_consumption_id", Integer(), primary_key=True),
            Column("capture_time", String(100)),
            Column("measured_consumption", Float(precision=2))
        )

    def save_data(self, record):
        my_insert = self._table.insert().values(
            capture_time = record.capture_time,
            measured_consumption = record.measured_consumption
        )

        self._connection.execute(my_insert)
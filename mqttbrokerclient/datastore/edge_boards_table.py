from sqlalchemy import Table, MetaData, Column, Integer, String, DateTime
from sqlalchemy.sql import select

from mqttbrokerclient.datastore.abstract_table import AbstractTable


class EdgeBoardsTable(AbstractTable):
    """
    Responsible for all transactions with "target_servers" table.
    """

    def create_table(self):
        metadata = MetaData()
        self._table = Table(
            "edge_boards",
            metadata,
            Column('board_id', Integer(), primary_key=True),
            Column('board_name', String(30)),
            Column('is_alive', Integer()),
            Column('last_contact_time', DateTime)
            )

    def get_all_records(self):
        selection = select([self._table])

        result_proxy = self._connection.execute(selection)

        result = result_proxy.fetchall()

        return result

    def check_for_new_records(self):
        raise Exception()

    def save_data(self):
        raise Exception()

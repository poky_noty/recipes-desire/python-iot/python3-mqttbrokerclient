"""
This module contains classes that are mapping records from `TargetSubnets` ...
table. The data that is retrieved from a database query in specific table ...
are stored into a Record object.
"""


class TargetSubnetsRecord(object):
    """
    Map records from `TargetSubnets` table to record-classes for integration ..
    with the rest of the monitoring application.
    """

    def __init__(self, subnet_id, subnet_cidr, subnet_name, is_new, is_handled, is_ready, board_id):
        self._subnet_id = subnet_id
        self._subnet_cidr = subnet_cidr
        self._subnet_name = subnet_name

        self._subnet_is_new = is_new
        self._subnet_is_handled = is_handled
        self._subnet_is_ready = is_ready

        self._subnet_board_id = board_id

    @property
    def subnet_id(self):
        """
        Getter method of property ID
        """
        return self._subnet_id

    @property
    def subnet_cidr(self):
        """
        Getter method of property CIDR
        """
        return self._subnet_cidr

    @property
    def subnet_name(self):
        """
        Getter method of property NAME
        """
        return self._subnet_name

    @property
    def subnet_is_new(self):
        """
        Getter method of property IS_NEW
        """
        return self._subnet_is_new

    @property
    def subnet_is_handled(self):
        """
        Getter method of property IS_HANDLED
        """
        return self._subnet_is_handled

    def __str__(self):
        """
        Create a custom string representation of this record object
        """
        return \
            "Record from `Target_Subnets` Table ... " \
            f"{self._subnet_id}, {self._subnet_cidr}, {self._subnet_name}, " \
            f"{self._subnet_is_new}, {self._subnet_is_handled}, " \
            f"{self._subnet_is_ready}, {self._subnet_board_id}"

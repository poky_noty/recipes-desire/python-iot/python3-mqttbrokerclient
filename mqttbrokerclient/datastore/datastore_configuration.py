import os
import logging

ALLOWED_TYPES = ["database", "messagequeue"]
DB_TARGETS = ["mysql"]
MQ_TARGETS = ["redis"]


class DatastoreConfiguration(object):
    """
    Handles external configuration data needed for esatblishing connnection ...
    with backend datastore.
    Save new data and query already stored records needs at the beginning ...
    the connect-with-this-data step.
    """

    def __init__(self):
        self.logger = logging.getLogger("mqttbroker-client")

        self.datastore_type = os.environ["DATASTORE-TYPE"]

        self.logger.debug("Datastore External Configuration is ... %s",
                          self.datastore_type)

    def evaluate(self):
        """
        Apply simple rules for ecaluating user entries for Datastore
        external configuration.
        """
        if not self.datastore_type:
            self.logger.error("Datastore External Configuration String is NULL"
                              " ... Aborting")

            raise ValueError("Datastore External Configuration String is NULL "
                             "... Aborting")

        my_parts = self.datastore_type.split("/")

        if len(my_parts) != 2:
            self.logger.error(
                "Datastore External Configuration String Error ... "
                "Split into too many parts {%d}", len(my_parts))

            raise ValueError("Datastore External Configuration String ... "
                             "Split into Too Many Parts")

        elif my_parts[0] not in ALLOWED_TYPES:
            self.logger.error(
                "Datastore External Configuration String Error ... "
                f"Unacceptable Type '{my_parts[0]}'"
            )

            raise ValueError("Datastore External Configuration String ... "
                             f"Unacceptable Type {my_parts[0]}")

        elif my_parts[1] not in DB_TARGETS and my_parts[1] not in MQ_TARGETS:
            self.logger.error(
                "Datastore External Configuration String Error ... "
                f"Unacceptable Target '{my_parts[1]}'")

            raise ValueError("Datastore External Configuration String ... "
                             f"Unacceptable Target '{my_parts[1]}'")

    def is_database(self):
        """
        Check whether the backend datastore that MQTT-Messages are stored
        permanently is of type Database.
        """
        my_parts = self.datastore_type.split("/")

        if my_parts[0].lower() == "database":
            return True

        return False

    def is_mysql(self):
        """
        In case of Database backend datastore verify whether it is of type
        MariaDB.
        """
        my_parts = self.datastore_type.split("/")

        if my_parts[1].lower() == "mysql":
            return True

        return False

    def is_message_queue(self):
        """
        Check whether the backend datastore that supports inter-container
        communication is of type MessageQueue.
        """
        my_parts = self.datastore_type.split("/")

        if my_parts[0].lower() == "messagequeue":
            return True

        return False

    def is_redis(self):
        """
        In case of message-queueu communication layer verify whether it is
        of type of Redis.
        """
        my_parts = self.datastore_type.split("/")

        if my_parts[1].lower() == "redis":
            return True

        return False


def get_database_connection_params():
    """
    The necessary parameters for a client to connect to a database are ...
    hostname, port, username, password,  db-name.

    Returns a structure containing all necessary parameters (defined ...
    externally)
    """
    host = os.environ['MARIADB-SERVER']
    user = os.environ["MARIADB_USER"]
    password = os.environ["MARIADB-PASSWORD"]
    dbname = os.environ["MARIADB_DBNAME"]

    return {
        "host": host,
        "user": user,
        "password": password,
        "dbname": dbname
    }

"""
This class contains needed code for all transactions with
TARGET_SERVERS table.
"""

from sqlalchemy import Table, MetaData, Column, Integer, String
from sqlalchemy.sql import select

#from mqttbrokerclient.datastore.abstract_table import AbstractTable
from .abstract_table import AbstractTable


class TargetServersTable(AbstractTable):
    """
    Responsible for all transactions with "target_servers" table.
    """

    def create_table(self):
        metadata = MetaData()
        self._table = Table(
            "target_servers",
            metadata,
            Column('server_id', Integer(), primary_key=True),
            Column('server_ip', String(30)),
            Column('server_name', String(30)),
            Column('is_new', Integer()),
            Column('is_handled', Integer()),
            Column('is_ready', Integer())
            )

    def get_all_records(self):
        """
        Retrieve ALL records from table.
        """
        selection = select([self._table])
        result_proxy = self._connection.execute(selection)
        result_array = result_proxy.fetchall()

        result_records = []
        for result in result_array:
            #result_array
            print(f"==>str({result})")

        return result_array

    def check_for_new_records(self):
        """
        Any records with attribute "is_new" set to True
        """
        selection = select([self._table]).where(self._table.c.is_new == 1)

        result_proxy = self._connection.execute(selection)

        record = result_proxy.first()

        assert record.server_id == 1

    def save_data(self):
        """
        Store new data to table
        """
        raise Exception()

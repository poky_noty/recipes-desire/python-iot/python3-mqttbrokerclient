"""
Contains the data to store to database .... insert to table for ac-power-consumption
"""
class ACPowerConsumptionRecord(object):
    """
    This class contains all data needed to create a record for table ac-power-consumption
    """
    def __init__(self, capture_time, measured_consumption):
        self._capture_time = capture_time
        self._measured_consumption = measured_consumption

    @property
    def capture_time(self):
        return self._capture_time

    @property
    def measured_consumption(self):
        return self._measured_consumption

    
"""
This module contains code that discovers new data for various devices  ...
stored in backend-datastore
"""

import logging

from mqttbrokerclient.datastore.db_manager import DBManager
#from mqttbrokerclient.datastore.target_subnets_table import TargetSubnetsTable
from .target_subnets_table import TargetSubnetsTable


class NewRecordsDiscovery(object):
    """
    This class is responsible for querying all important tables in database ...
    and locate new records that need further actions
    """

    def __init__(self):
        self._logger = logging.getLogger("mqttbroker-client")
        self._db_manager = DBManager()

    def find_all_new(self):
        """
        Find new records into all database tables.
        """
        self.scan_target_subnets_table()
        self.scan_edge_boards_table()

    def scan_target_subnets_table(self):
        """
        Scans database for new records in target-subnets table
        """
        new_subnets = self._db_manager.get_new_subnets_records()

    def check_target_servers_table(self):
        """
        Scans the database for new target_servers stored
        """
        pass

    def scan_edge_boards_table(self):
        """
        Scans the database for new edge_boards stored
        """
        pass

    def __del__(self):
        print("!!! Now I will leave away !!!")

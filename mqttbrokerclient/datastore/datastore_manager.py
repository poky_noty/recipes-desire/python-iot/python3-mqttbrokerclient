"""
A top-level manager that handles read/ write transactions with datastore
no matter type.
This module is responsible to prepare the approprate requests and hide
implementation implications if message-queue or database.
"""

import logging

from mqttbrokerclient.external_configuration_shovel \
    import ExternalConfigurationShovel
from mqttbrokerclient.datastore.db_manager import DBManager
from mqttbrokerclient.datastore.redis_manager import RedisManager
from mqttbrokerclient.datastore.unknown_datastore \
    import UnknownDatastoreManager


class DatastoreManager(object):
    """
    Exposes the most important functionality ...
    the user is NOT bothered with the kind of backend storage service exists
    """

    def __init__(self):
        self.logger = logging.getLogger("mqttbroker-client")

        self.data_manager = None
        self.__get_kindof_trasnslator()

    def __get_kindof_trasnslator(self):
        """
        Depending on application-context (is it running centrally or locally)
        an appropriate translator is returned ...
        database as backend datastore --> An SQL tramnslator is needed
        message-queue for inter-communication --> A Key/ Value
          translator is needed
        """
        external_configuration_shovel = ExternalConfigurationShovel()

        datastore_configuration = external_configuration_shovel.\
            get_datastore_configuration()

        if datastore_configuration.is_database():
            # Datastore type is .... Database
            self.data_manager = DBManager()
        elif datastore_configuration.is_message_queue():
            # Datastore type is ... messagequeue
            self.data_manager = RedisManager()
        else:
            # Datastore type is ... Unknown
            raise ValueError("Cannot Handle this Type of DataStore ... "
                             "Aborting")

    def save_data(self):
        """
        Save the payload of a new MQTT-Message to backend datastore ...
        """
        pass

    def read_records(self):
        """
        Read records from a backend datastore ...
        In case of administrator the GUI updates the sys-configuration ...
        and the edge boards must be informed with MQTT-Messages.
        """
        pass

    def test_connection(self):
        """
        Try to establish a connection with datastore.
        In case of success external configuration is OK and ...
        Proceed with Data Storing & Querying.
        """
        if isinstance(self.data_manager, DBManager):
            self.logger.debug("Ready to Verify Successful Connection with "
                              "Database ...")

            self.data_manager.connect_or_raise_exception()
        elif isinstance(self.data_manager, RedisManager):
            self.logger.debug("Ready to Verify Succesful Connection with "
                              "MessageQueue ...")

            self.data_manager.connect_or_raise_exception()
        else:
            raise UnknownDatastoreManager()

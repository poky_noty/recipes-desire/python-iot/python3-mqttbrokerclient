def create_json_body_for_data_point(measurement, time, host, region, topic_id, value):
    json_body = None
    
    if "status" in measurement:
        json_body = create_json_body_for_status_data_point(measurement, time, host, region, topic_id, uptime=value)
    elif "temperature" in measurement and "humidity" in measurement:
        json_body = create_json_body_for_temperature_humidity_sensor_data_point(measurement, time, host, region, topic_id, value)
    else: 
        json_body = create_json_body_for_sensor_data_point(measurement, time, host, region, topic_id, float_value=value)

    return json_body

def create_json_body_for_sensor_data_point(measurement, time, host, region, topic_id, float_value):
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "topic-id": topic_id,
                "host": host,
                "region": region
            },
            "time": time,
            "fields": {
                "float_value": float_value
            }
        }
    ]
    
    return json_body

def create_json_body_for_status_data_point(measurement, time, host, region, topic_id, uptime):
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "topic-id": topic_id,
                "host": host,
                "region": region
            },
            "time": time,
            "fields": {
                "uptime": uptime
            }
        }
    ]
    
    return json_body

def create_json_body_for_temperature_humidity_sensor_data_point(measurement, time, host, region, topic_id, temperature_humidity_list):
    
    temperature_value = temperature_humidity_list.get("temperature")
    humidity_value = temperature_humidity_list.get("humidity")
    temperature_unit = temperature_humidity_list.get("temperature-unit")

    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "topic-id": topic_id,
                "host": host,
                "region": region
            },
            "time": time,
            "fields": {
                "temperature": temperature_value,
                "humidity": humidity_value,
                "temperature-unit": temperature_unit 
            }
        }
    ]

    return json_body
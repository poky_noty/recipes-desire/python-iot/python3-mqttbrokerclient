import os

from influxdb import InfluxDBClient

def get_connection():
    host_port = os.environ["INFLUXDB-SERVICE"]
    
    parts = host_port.split(":")
    
    host = parts[0]
    port = int(parts[1])
    database = parts[2]

    client = InfluxDBClient(host=host, port=port) #, database=database)

    dbs = client.get_list_database()

    client.switch_database(database=database)

    measurements = client.get_list_measurements()

    return client
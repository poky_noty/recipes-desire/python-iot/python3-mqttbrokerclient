import redis

class RedisManager(object):
    def __init__(self):
        pass

def establish_single_connection(logger, host, port, db_number):
    logger.debug("Trying to establish a network connection with Redis")
    try:
        client = redis.StrictRedis(host=host, port=port, db=db_number)
    except redis.ConnectionError as conn_err:
        logger.error("Failed Redis Connection ... %s", str(conn_err))
        return None

    logger.debug("Successful Connection & Client is %s", client)

    # The test
    ping = client.ping()

    logger.debug("Successful Test & Ping is %s", ping)
    logger.debug("Redis Server Info is ['Version']=%s",
                 client.info().get("redis_version"))

    return client

def set_single_key_value(client, key="dummy", value="empty"):
    #logger.debug("Ready to Set a Single Key/ Value ... %s / %s", key, value)
    response = client.set(key, value)
    #logger.debug("Successful Set Response is %s", response)

    return response


def get_single_key_value(client, key):
    #logger.debug("Ready to Get a Single Key/ Value ... %s / ?", key)
    response = client.get(key)
    #logger.debug("Successful Get Response is %s", response)

    return response

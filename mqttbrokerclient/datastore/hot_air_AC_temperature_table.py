"""
This module contains classes associated with hot_air_AC_temperature table.
Is able to store and tetrieve records for specific table.
"""

from sqlalchemy import Table, MetaData, Column, Integer, String, Numeric

from .abstract_table import AbstractTable

class HotAirACTemperatureTable(AbstractTable):
    """
    This class is responsible for all transactions with "hot_air_AC_temperature" table.
    Is able to store and collect records from backend-database specific table.
    """

    def get_all_records(self):
        self._logger.debug("Not Yet Implemented !")

    def create_table(self):
        self._logger.debug("Ready to create the ORM with a class that has as attributes the table columns !")

        metadata = MetaData()
        self._table = Table(
            "hot_air_AC_temperature",
            metadata,
            Column('temp_rec_id', Integer(), primary_key=True),
            Column('capture_time', String(80)),
            Column('measured_temperature', Numeric(4,2))
        )

    def save_data(self, record):
        self._logger.debug("This is the last station before Database ... All passengers will be headed to Table Hot-Air-AC-Temperature")

        if record is None:
            raise Exception("Aborting Save_Data() in Hot_Air_AC_Temperature Table!")

        ins = self._table.insert().values(
            capture_time=record.capture_time,
            measured_temperature=record.measured_temperature
        )

        self._connection.execute(ins)

"""
Transactions with CPU_UTILIZATION_METRICS table.
"""

from sqlalchemy import MetaData, Table, Column, Integer, String, Float, DateTime

from .abstract_table import AbstractTable


class CPUStatsTable(AbstractTable):
    """
    This class stores and retrieves the CPU-Utilization-Metrics that ...
    edge-boards provide every X secs.
    """

    def create_table(self):
        """
        Creates an sql-alchemy TABLE object that is associated with ...
        database table CPU-UTILIZATION-METRICS.
        """
        metadata = MetaData()
        self._table = Table(
            "CPU_UTILIZATION_METRICS",
            metadata,
            Column('metric_id', Integer(), primary_key=True),
            Column('board_id', Integer()),
            Column('cpu_TIMES', String(200)),
            Column('cpu_PERCENT', Float()),
            Column('cpu_COUNT', Integer()),
            Column('cpu_STATS', String(200)),
            Column('cpu_LOADAVG', String(200)),
            Column('metric_datetime', DateTime())
            )

    def save_data(self, new_metrics):
        pass

    def get_all_records(self):
        pass

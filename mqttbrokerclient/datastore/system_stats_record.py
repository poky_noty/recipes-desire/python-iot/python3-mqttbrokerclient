import logging

class SystemStatsRecord(object):
    def __init__(self, measurement, time_taken):
        self._measurement = measurement
        self._time_taken = time_taken

        self.logger = logging.getLogger("mqttbroker-client")
        self.logger.debug("Inside the SystemStatistics Data-Object")

    @property
    def measurement(self):
        return self._measurement

    @property
    def time_taken(self):
        return self._time_taken

    def __str__(self):
        return self._measurement

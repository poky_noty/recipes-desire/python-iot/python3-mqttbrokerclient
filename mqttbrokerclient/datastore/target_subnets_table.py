"""
In order to find Up & Running Serrvers, a Subnet-CIDR is needed.
When the interesting cidr is stored into database, the monitoring software is
using scanning tools.
The necessary transaction of data storage and query are handled by this class.
"""

from sqlalchemy import Table, MetaData, Column, Integer, String
from sqlalchemy.sql import select

#from mqttbrokerclient.datastore.abstract_table import AbstractTable
from mqttbrokerclient.datastore.abstract_table import AbstractTable
#from mqttbrokerclient.datastore.target_subnets_record \
#  import TargetSubnetsRecord
from .target_subnets_record import TargetSubnetsRecord


class TargetSubnetsTable(AbstractTable):
    """
    Responsible for all transactions with "target_subnets" table.
    """

    def create_table(self):
        metadata = MetaData()
        self._table = Table(
            "target_subnets",
            metadata,
            Column('subnet_id', Integer(), primary_key=True),
            Column('subnet_cidr', String(50)),
            Column('subnet_name', String(30)),
            Column('is_new', Integer()),
            Column('is_handled', Integer()),
            Column('is_ready', Integer()),
            Column('board_id', Integer())
            )

    def get_all_records(self):
        """
        Retrieve all records from TARGET_SUBNETS table
        """

        selection = select([self._table])

        result_proxy = self._connection.execute(selection)

        results = result_proxy.fetchall()

        subnet_records = []
        for result in results:
            # Attributes of subnet
            subn_id = result.subnet_id
            subn_cidr = result.subnet_cidr
            subn_name = result.subnet_name
            subn_is_new = result.is_new
            subn_is_handled = result.is_handled
            subn_is_ready = result.is_ready
            subn_board_id = result.board_id

            subnet_record = TargetSubnetsRecord(
                subn_id, subn_cidr, subn_name, subn_is_new,
                subn_is_handled, subn_is_ready, subn_board_id)

            # self._logger.debug(str(subnet_record))

            subnet_records.append(subnet_record)

        return subnet_records

    def check_for_new_records(self):
        """
        Any records with attribute "is_new" set to True
        """
        selection = select([self._table]).where(self._table.c.is_new == 1)

        result_proxy = self._connection.execute(selection)

        record = result_proxy.first()

        assert record.server_id == 1
        raise Exception()

    def save_data(self):
        raise Exception()

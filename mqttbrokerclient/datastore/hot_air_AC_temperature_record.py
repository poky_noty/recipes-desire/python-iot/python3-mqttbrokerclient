"""
This module contains classes that represent a compact form of HOT_AIR_AC_TEMPERATURE table records 
"""

class HotAirACTemperatureRecord(object):
    """
    This class has attributes that correspond to HOT-AIR-AC-TEMPERATURE table ...
    """
    def __init__(self, capture_time, measured_temperature):
        self._captute_time = capture_time
        self._measured_temperature = measured_temperature

    @property
    def capture_time(self):
        return self._captute_time

    @property
    def measured_temperature(self):
        return self._measured_temperature
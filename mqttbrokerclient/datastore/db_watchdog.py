"""
The watchdog is a background process ...
Its purpose is to discover recently-added records into the database ...
In case of new entries the rest of the application will be notified ...
"""
import logging

from time import sleep

from mqttbrokerclient.datastore.new_records_discovery import NewRecordsDiscovery

import os

class DBWatchdog(object):
    """
    This class is responsible for monitoring the backend datastorage
    """

    def __init__(self, polling_interval):
        self._polling_interval = polling_interval
        self._continue_polling = True

    def start_monitoring(self):
        """
        Start the collecytion of database performance statistics and other ...
        important data.
        """
        self._apply_polling_cycle()

    def _apply_polling_cycle(self):
        """
        Schedules next visit to database for new records arrival
        """
        while True:
            # Goto database and check records status
            discovery = NewRecordsDiscovery()
            discovery.find_all_new()

            sleep(self._polling_interval)

            if not self._continue_polling:
                break

    @property
    def polling_interval(self):
        """
        Returns the polling frequency for database status
        """
        return self._polling_interval

    def set_continue_polling(self, value):
        """
        Change the value of 'continue_polling' member variable
        """
        self._continue_polling = value

import redis

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!! RedisWatchdog for monitoring changes in MessageQueue like publishing in specific topic !!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
class RedisWatchdog(object):
    """
    Monitors changes in Redis MessageQueue and acts to messages sent between microservices
    """
    def __init__(self):
        host = os.environ["REDIS-HOST"]
        self.host = host
        
        port = os.environ["REDIS-PORT"]
        self.port = port
        
        db = os.environ["REDIS-DB"]
        self.db = db 

        self.r = None
        self.p = None
        self.thread = None

        self.logger = logging.getLogger("redis-watchdog")

        return

    def try_to_connect(self):
        """
        Try to establish connection with Redis message-queue
        """
        self.r = redis.Redis(host=self.host, port=self.port, db=self.db)
        if self.r is None:
            raise Exception("Failed to connect to Redis Server")

    def subscribe_to_channel_and_listen(self, *argv):
        """
        Create a PubSub instance to listen for new messages.
        """
        self.p = self.r.pubsub()
        
        for arg in argv:
            # Channels and patterns list can be subscribed to
            my_channel = arg
            
            if "network" in my_channel: 
                my_handler = custom_network_handler

            self.p.subscribe(**{my_channel: my_handler})

        self.thread = self.p.run_in_thread(sleep_time=0.001)
    
def custom_network_handler(message):
    """
    What will happen in case of new message in subscribed channels
    """



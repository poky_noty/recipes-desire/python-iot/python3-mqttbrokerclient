"""
This module contains the code that is executed when new records ...
are inserted into the application database.
"""
from .target_subnets_record import TargetSubnetsRecord


class NewRecordsHandler(object):
    """
    """

    def __init__(self):
        pass

    def handle_new_records(self, new_rec):
        if isinstance(new_rec, TargetSubnetsRecord):
            # publish new-subnet message
            self.handle_new_subnet()
        else:
            pass

    def handle_new_subnet(self):
        # Call appropriate mqtt-Publisher
        pass

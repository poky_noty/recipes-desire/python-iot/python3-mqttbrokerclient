"""
A custom  excpeption --> Thrown in case of a database that is not supported.
The monitoring application cannot --> Prepare appropriate SQL statements.
"""


class UnsupportedDatabase(Exception):
    """
    The application uses a database for permanent storage of data ...
    Needs to prepare the approprate SQL statements for querying and storing ...
    Cannot proceed with database transactions if unknown SQL dialect ...
    """

    def __init__(self, msg):
        super().__init__(self, msg)

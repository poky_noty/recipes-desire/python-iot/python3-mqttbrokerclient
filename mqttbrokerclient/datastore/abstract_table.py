from abc import ABC, abstractmethod

from sqlalchemy.sql import select

import logging


class AbstractTable(ABC):
    """
    Template for all classes that handle db-tables transactions
    """

    def __init__(self, _connection):
        """
        Initialize basic attributes ...
        The table that this class is responsible of.
        """
        self._logger = logging.getLogger("mqttbroker-client")

        self._table = None
        self._connection = _connection

        self.create_table()

        super().__init__()

    @abstractmethod
    def get_all_records(self):
        """
        Collects all table records ... no filtering applied
        """
        pass

    def check_for_new_records(self):
        """
        Check table rows to locate new records.
        Useful for a polling process.
        """

        # Prepare the query
        selection = select([self._table]).where(self._table.c.is_new == 1)

        # Execute query
        result_proxy = self._connection.execute(selection)

        # Return the result-set of new-records query
        return result_proxy

    @abstractmethod
    def save_data(self):
        """
        Insert new records into the table.
        """
        pass

    @abstractmethod
    def create_table(self):
        """
        Create the table in case of a fresh database.
        """
        pass

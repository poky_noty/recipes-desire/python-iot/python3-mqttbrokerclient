from mqttbrokerclient.datastore.abstract_table import AbstractTable
 
class StartStopServerTable(AbstractTable):
    def __init__(self):
        """
        @id
        @serverID
        @action
        @request_time
        @handling_time
        @response_time
        @status
        """

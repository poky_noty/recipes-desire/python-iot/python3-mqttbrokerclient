"""
This class handles all transactions with "dummy_countries" table.
It is an implementation of AbstractTable that contains a set of ...
common functions for storying and querying data.
"""

from sqlalchemy import Table

from mqttbrokerclient.datastore.abstract_table import AbstractTable


class DummyCountriesTable(AbstractTable):
    """
    The class that is responsible for "dummy_countries" table.
    """

    def __init__(self):
        super().__init__(self)

        self.create_table()

    def create_table(self):
        self.table = Table()


    def check_for_new_records(self):
        pass

    def save_data(self):
        pass

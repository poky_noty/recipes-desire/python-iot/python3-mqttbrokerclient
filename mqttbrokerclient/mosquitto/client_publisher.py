"""
This module knows all details for establishing a connection to an mqtt-broker
and creates a simple  publisher that sends new messages containing
newly-collected data.
"""

import os
import logging

import paho.mqtt.client as mqtt


class ClientPublisher(object):
    """
    The component of the application that is sending MQTT messages ...
    When building IoT dashboards you need to publish both system-adminstration
    messages and sensor data using different topics.
    """

    def __init__(self, is_admin=None, is_sensordata=None):
        """
        Initialize the subscriber-class with appropriate configuration-data ...
        to establish connection with mqtt-broker and
        subscribe to specific topic.
        """
        self.logger = logging.getLogger('mqttbroker-client')

        self.hostname = os.environ["MOSQUITTO_HOST"]

        self.port = os.environ["MOSQUITTO_PORT"]
        self.port = int(self.port)

        if is_admin:
            self.topic = os.environ["MQTT-ADMIN-TOPIC"]
        elif is_sensordata:
            self.logger.debug("**** Performance Data --> Need Topic Two *****")
            self.topic = os.environ["MQTT-DATA-TOPIC"]

        self.mqttc = None

    def on_publish(self, mqttc, obj, mid):
        """
        Callback method ... in case of new publish request
        """
        self.logger.debug(
            "Ready to Publish New MQTT-Message ... "
            f"Message ID = {mid}")

    def on_log(self, mqttc, obj, level, string):
        """
        Event: Log information is available ... This callback will be triggered
        """
        self.logger.debug(
            "MQTT-Simple-Publisher:  Log Information is Available"
            f"\n .... {string}")

    def on_connect(self, mqttc, obj, flags, rc):
        """
        Event: Connection is Acknowledged ... This callback will be triggred
        """
        self.logger.debug(
            "MQTT-Simple-Publisher: Connection with broker is Acknowledged\n")

    def on_message(self, mqttc, obj, msg):
        """
        This callback is called in response to event ...
        new-message-arrived in broker
        """
        self.logger.info(
            "MQTT-Simple-Publisher: Message Received !\n"
            f" .... Message Payload = {str(msg.payload)}")

    def connect_to_broker(self):
        """
        Connect to an MQTT-Broker and start communicating with other
        application components. The broker is primarily responsible for
        receiving all messages and publishing them to interested subscribers.
        """
        self.mqttc = mqtt.Client()
        self.mqttc.enable_logger(self.logger)

        # Callbacks in case of events
        self.mqttc.on_connect = self.on_connect
        self.mqttc.on_publish = self.on_publish
        self.mqttc.on_message = self.on_message
        self.mqttc.on_log = self.on_log

        # Validate needed parameters
        if not self.mqttc:
            # Undefined
            raise TypeError(
                "Cannot Connect to MQTT-Broker ... Undefined Client")
        elif not self.hostname or not isinstance(self.hostname, str):
            # Undefined Hostname .. Aborting
            raise TypeError(
                "Cannot Connect to MQTT-Broker ... Undefined Hostname")
        elif not self.port or not isinstance(self.port, int):
            # Wrong Port
            raise TypeError("Cannot Connect to MQTT-Broker ... Undefined Port")

        self.logger.debug("Ready to Connect with Broker ... %s:%d",
                          self.hostname, self.port)

        # Establish connection
        try:
            self.mqttc.connect(self.hostname, self.port, keepalive=60)

            # Successful connection with mqtt-broker
            self.logger.debug(
                "MQTT-Client Successfully Connected with Broker !")

        except Exception as conn_e:
            self.logger.error(
                "Connection with MQTT-Broker raised an exception !\n%s",
                str(conn_e))

            raise ConnectionError(
                "Connecting to MQTT-Broker Raised an Exception ... Aborting")

    def publish_and_wait(self, msg="Hello MQTT from Athens"):
        """
        Subscribes the client to a topic - best to be short and consise.
        A message is published to subscribed-topic.
        """
        if not self.topic or not isinstance(self.topic, str):
            # Need a TOPIC to subscribe to
            raise TypeError(
                "Cannot Publish to MQTT-Broker ... Undefined Topic")

        """    try:
            self.logger.debug(
                f"Ready to Subscribe MQTT-Client to Topic {self.topic}")

            self.mqttc.subscribe(self.topic, qos=2)

            self.logger.debug(
                f"MQTT-Client Successfully Subscribed to Topic ...{self.topic}"
                "\nNow Waiting for Messages to be Published")

        except Exception as subs_e:
            self.logger.error(
                "Failed to Subscribe to Topic ... Exception is Raised !\n%s",
                str(subs_e))

            raise ConnectionError(
                "Topic Subscription Raised an Exception")

        ####
        # ### Successfully subscribed to topic
        # ### Now try to send the mqtt-message
        ####
        self.mqttc.loop_start()

        if msg is None:
            msg = "**************"
        else:
            msg2 = str(msg.__class__.__name__)

        """
        msg = str(msg)
        self.logger.debug(
            "MQTT-Simple-Publisher ... Ready to Publish !\n"
            f" .... Message = {msg}/{self.topic}")

        #self.topic = "test3"
        #msg = "Xtra"
        infot = self.mqttc.publish(self.topic, msg, qos=0)

        # Publish a second message immediately after the first one
        # self.logger.debug("from Athens, Greece")
        # infot = self.mqttc.publish(self.topic, msg + ", Greece", qos=2)

        #infot.wait_for_publish()

        self.logger.debug(
            "Simple-MQTT-Publisher ... Message was Published Successfully !\n"
            f" ... Message = {msg}")

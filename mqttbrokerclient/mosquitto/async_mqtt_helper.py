"""
Try to connect with mqtt-broker using an ASYNCIO task
"""

class AsyncioHelper(object):
    """
    Helper class that defines the asyncio task ---
    MQTT-Broker connection & subscription will run independently ...
    Application will not block here.
    """

    def __init__(self):
        pass

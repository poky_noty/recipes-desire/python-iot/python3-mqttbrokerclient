import paho.mqtt.client as mqtt

# Print all messages using the following ... Logger
#import logging
#logging.basicConfig(level=logging.DEBUG)
#logger = logging.getLogger(__name__)


class MqttConnection:
    is_connected = False
    is_disconnected = False
    logger = None

    def __init__(self, logger, host, port=1883, keepalive=60):
        self.host = host
        self.port = port
        self.keepalive = keepalive
        self.logger = logger

    # The callback for when the client recieves a CONNACK response from server.
    def on_connect(self, client, userdata, flags, response_code):
        self.logger.debug(
            "Successful Connection to MQTT-Broker ...\n" +
            "Client = %s\nUserData = %s\nFlags = %s\nResponse=%s", client,
            userdata, flags, response_code)

        self.is_connected = True
        self.is_disconnected = False

    # The callback for when a PUBLISH message is recieved from the server
    def on_message(self, userdata, msg):
        self.logger.debug(
            "On Message Callback...\n" + "UserData = %s\nMessage = %s",
            userdata, msg)

    # The callback for when connection is closed
    def on_disconnect(self, client, userdata, response_code):
        self.logger.debug(
            "On Disconnect Callback ...\n" +
            "Client = %s\nUserData = %s\nResponse=%s", client, userdata,
            response_code)

        self.is_connected = False
        self.is_disconnected = True

    def connect_client_blocking(self):
        self.logger.debug(
            "Ready to connect into Mosquitto Mqtt-Broker ... %s:%d", self.host,
            self.port)

        client = mqtt.Client()

        client.on_connect = self.on_connect
        client.on_message = self.on_message
        client.on_disconnect = self.on_disconnect

        client.enable_logger(logger=self.logger)

        # Important: Running using docker-compose and client connects to pokky-mosquitto
        #  Even if there is a port-mapping 1984:1883 ... the python-code should connect to 1883 !!!!
        #  Avoid using 1984 from another co-hosted docker container.
        client.connect(self.host, self.port, self.keepalive)

        return client

    def isconnected(self):
        return self.is_connected

    def isdisconnected(self):
        return self.is_disconnected

import os
import logging
import asyncio
import socket
import paho.mqtt.client as mqtt

from mqttbrokerclient.mechanisms.linkages.simple_mqtt_client_support \
    import parse_new_message_and_find_handler, save_all_sensors_status_table, handle_sensor_status_response

class AsyncioHelper:
    def __init__(self, loop, client):
        self.loop = loop
        self.client = client
        self.client.on_socket_open = self.on_socket_open
        self.client.on_socket_close = self.on_socket_close
        self.client.on_socket_register_write = self.on_socket_register_write
        self.client.on_socket_unregister_write = self.on_socket_unregister_write

    def on_socket_open(self, client, userdata, sock):
        print("Socket opened")

        def cb():
            print("Socket is readable, calling loop_read")
            client.loop_read()

        self.loop.add_reader(sock, cb)
        self.misc = self.loop.create_task(self.misc_loop())

    def on_socket_close(self, client, userdata, sock):
        print("Socket closed")
        self.loop.remove_reader(sock)
        self.misc.cancel()

    def on_socket_register_write(self, client, userdata, sock):
        print("Watching socket for writability.")

        def cb():
            print("Socket is writable, calling loop_write")
            client.loop_write()

        self.loop.add_writer(sock, cb)

    def on_socket_unregister_write(self, client, userdata, sock):
        print("Stop watching socket for writability.")
        self.loop.remove_writer(sock)

    async def misc_loop(self):
        print("misc_loop started")
        while self.client.loop_misc() == mqtt.MQTT_ERR_SUCCESS:
            try:
                await asyncio.sleep(1)
            except asyncio.CancelledError:
                break
        print("misc_loop finished")


class ClientSubscriber(object):
    """
    The component of the application that is receiving MQTT messages ...
    When building IoT dashboards you need to handle both system-adminstration
    messages and lightweight sensor data
    """

    def __init__(self, loop, is_admin=False, is_sensordata=False):
        """
        Initialize the subscriber-class with appropriate configuration-data ...
        to establish connection with mqtt-broker and
        subscribe to specific topic.
        """
        self.loop = loop

        self.logger = logging.getLogger('mqttbroker-client')

        self.hostname = os.environ["MOSQUITTO_HOST"]

        self.port = os.environ["MOSQUITTO_PORT"]
        self.port = int(self.port)

        self.logger.debug(f"What is the Topic for this Subscriber ?")
        if is_admin:
            self.logger.debug(f"ADMIN Topic Defined")
            self.topic = os.environ["MQTT-ADMIN-TOPIC"]
        elif is_sensordata:
            self.logger.debug("SENSORDATA Topic Defined")
            #self.topic = os.environ["MQTT-DATA-TOPIC"]
            topics_all = os.environ["MQTT-DATA-TOPIC"]
            self.topics_list  = topics_all.split(",")

            self.logger.debug(f"Topics List is ...{self.topics_list}")

            #self.topic = topics_list[0]

        self.mqttc = mqtt.Client()
        self.mqttc.enable_logger(self.logger)

    def on_connect(self, client, userdata, flags, rc):
        """
        Callback to run in case of successful connection of client
        with MQTT-broker.
        """
        self.logger.debug(
          "Inside ON-CONNECT callback :\n"
          "(1)MQTT-Client Successful Connection with Broker to Subscribe\n"
          f"(2)Topics are ... {self.topics_list}"
          )

        # SOOOOOOOOOOS !!!!
        #self.topic = "test3"
        for topic in self.topics_list:
            self.logger.debug(f"Ready to Subscribe to Topic ... {topic}")
            client.subscribe(topic, qos=0)

        #client.subscribe("admin/keepalive/")

    def on_message(self, client, userdata, msg):
        self.logger.debug("***** --> %s ############### %s *****", str(msg.payload), str(msg.topic))

        # Send received message to appropriate parser and handler
        try:
            all_interesting_device = os.environ["INTERESTING-DEVICE"]
            self.logger.debug(f"I will handle messages ONLY from device '{all_interesting_device}'")

            nice_devices = all_interesting_device.split(",")

            for interesting_device in nice_devices:
                # A nice device that I want to parse the mqtt-message
                
                mqtt_msg = str(msg.payload)
                     
                if interesting_device in msg.topic:
                    # This is an mqtt-message published from interesting-device ... Need to handle it
                    parse_new_message_and_find_handler(msg.topic, mqtt_msg)
                
                # Drop message .. not interested on that device
                #self.logger.debug(f"Message will be droped ... Coming from a Non-Interesting Device {interesting_device} / {msg.topic}")

            if "STATE" in msg.topic:
                # There are tables that contain data for all sensors .. Not only interesting ones
                save_all_sensors_status_table(msg.topic, mqtt_msg)


            if "STATUS" in msg.topic:
                #self.logger.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                handle_sensor_status_response(str(msg.topic), str(msg.payload))

            if "LWT" in msg.topic:
                self.logger.debug(f"Found an LWT Mqtt-Message ... {msg.topic} / {str(msg.payload)}")

        except Exception as ex:
            self.logger.error("Exception is raised during on_message ... %s", str(ex))
            return

        #if not self.got_message:
        #    try:
        #        print("Got unexpected message: {}".format(msg.decode()))
        #    except Exception as e_xcep:
        #        self.logger.error(f"Exception is raised {e_xcep}")
        #else:
        #    self.got_message.set_result(msg.payload)

    def on_disconnect(self, client, userdata, rc):
        self.disconnected.set_result(rc)

    def on_admin_keepalive(self, mosq, obj, msg):
        print("Received Keep-Alive Message from EdgeBoard")

    def on_admin_network(self, mosq, obj, msg):
        print("Received Network-Problem Message")

    def on_log(self, mqttc, obj, level, string):
        """
        Event: Log information is available ... This callback will be triggered
        """
        self.logger.debug(
            "MQTT-Simple-Subscriber:  Log Information is Available"
            f"\n .... {level}/{string}")

    def on_publish(self, client, userdata, mid):
        """
        Event: When is it called ?
        """
        self.logger.debug("---> *** King Arthur: Legend of the Sword (2017) *** <---")

    async def main(self):
        self.logger.debug("!!! Joker now on cinemas !!!")

        self.disconnected = self.loop.create_future()
        self.got_message = None

        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_publish = self.on_publish
        self.client.on_message = self.on_message
        self.client.on_log = self.on_log
        self.client.on_disconnect = self.on_disconnect

        #self.client.message_callback_add("admin/keepalive/", self.on_admin_keepalive)
        #self.client.message_callback_add("admin/network/#", self.on_admin_network)

        aioh = AsyncioHelper(self.loop, self.client)

        #self.client.connect('iot.eclipse.org', 1883, 60)
        self.logger.debug(
            f"Ready to Connect to MQTT-Broker --> host = {self.hostname} & "
            f"port = {self.port}")
        try:
            self.client.connect(self.hostname, self.port, 60)
            #self.client.connect('mqtt.eclipse.org', 1883, 60)
        except Exception as ex:
            self.logger.error(
                f"Connect to MQTT-Broker Raised an Exception --> {str(ex)}")
            raise Exception("Failed to Connect to MQTT-Broker ... Aborting !")
        self.logger.debug("Successful Connection to MQTT-Broker")

        self.client.socket().setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 2048)

        #for c in range(3):
        while True:
            await asyncio.sleep(25)
            #print("Publishing")
            self.got_message = self.loop.create_future()

            #self.client.publish(self.topic, b'Hello' * 40000, qos=1)
            #self.client.publish("admin/keepalive/", b'Hello' * 40000, qos=1)
            self.logger.debug("****************@@@@@@@@@@@@@@@@@@@@@@@@@@@@****************")
            #self.client.publish("cmnd/line/lamp1/Power", "ON", qos=0)
            self.client.publish("cmnd/line/lamp1/Status", "0", qos=0)

            #msg = await self.got_message
            
            #self.logger.debug("*********************************************@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
            #print("Got response with {} bytes".format(len(msg)))
            #self.got_message = None

        self.client.disconnect()
        print("Disconnected: {}".format(await self.disconnected))

"""
    def connect_to_broker(self):
        if not self.mqttc:
            # Undefined
            raise TypeError(
                "Cannot Connect to MQTT-Broker ... Undefined Client")
        elif not self.hostname or not isinstance(self.hostname, str):
            # Undefined Hostname .. Aborting
            raise TypeError(
                "Cannot Connect to MQTT-Broker ... Undefined Hostname")
        elif not self.port or not isinstance(self.port, int):
            # Wrong Port
            raise TypeError("Cannot Connect to MQTT-Broker ... Undefined Port")

        # #########
        # Ready to establish connection with MQTT-Broker
        self.logger.debug("Ready to Connect with Broker ... %s:%d",
                          self.hostname, self.port)

        try:
            self.mqttc.connect(self.hostname, self.port, keepalive=60)

            # Successful connection with mqtt-broker
            self.logger.debug(
                "MQTT-Client Successfully Connected with Broker !")

        except Exception as conn_e:
            self.logger.error(
                "Connection with MQTT-Broker raised an exception !\n%s",
                str(conn_e))

            raise ConnectionError(
                "Connecting to MQTT-Broker Raised an Exception ... Aborting")

    def subscribe_to_topic(self):
        if not self.topic or not isinstance(self.topic, str):
            # Need a TOPIC to subscribe to
            raise TypeError(
                "Cannot Connect to MQTT-Broker ... Undefined Topic")

        try:
            # #######################################
            # Subscription process is .. Launching
            self.logger.debug(
                f"Ready to Subscribe MQTT-Client to Topic {self.topic}")

            self.mqttc.subscribe(self.topic, qos=0)

            self.logger.debug(
                "MQTT-Client is Successfully Subscribed to Topic ...\n"
                "Now Waiting for Messages to be Published")

        except Exception as subs_e:
            self.logger.error(
                "Failed to Subscribe to Topic ... Exception is Raised !\n%s",
                str(subs_e))

            raise ConnectionError(
                "Topic Subscription Raised an Exception")

        self.mqttc.loop_forever()
"""

def nice_main():
    loop = asyncio.get_event_loop()
    loop.run_forever(ClientSubscriber(loop).main())
    loop.close()

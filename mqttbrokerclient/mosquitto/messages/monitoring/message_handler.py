"""
Handle new mqtt messages ... just arrived in Broker
"""

import logging

from mqttbrokerclient.mechanisms.linkages.database_transactions_assistant import DBTransAssistant

from .message_parser import MessageParser

class MessageHandler(object):
    """
    Many kind of messages arrive at broker ....
    After parsing need to decide what actions need to be taken ...
    Possible actions are: Database/ Storage, Telegram/ Notifications, ...
    """

    def __init__(self, measurement=None, the_time=None, parts=None, topic_id=None):
        self.measurement = measurement
        self.the_time = the_time

        self.parts = parts

        self.topic_id = topic_id

        # The database aware component
        # self.db_assistant = DBTransAssistant()
        
        self.logger = logging.getLogger("mqttbroker-client")

    def save_to_db(self):
        """
        A message just arrived with important sensor/ monitoring data ...
        Need to store payload to database ... someone should know which table
        """
        db_assistant = DBTransAssistant()
        db_assistant.save_system_stats(self.measurement, self.the_time)

    def send_notification_to_all_users(self, number_times=1):
        """
        In case of important conditions appearing, try to conatact interested
        parties ... send an informative message for X number of times.
        """
        pass

    def handle_tasmoto(self):
        """
        Take care of a Tasmoto device message 
        """
        measurement_time = self.parts[0]
        self.logger.debug(f"Tasmoto message --> TIME = {measurement_time}")

        energy_or_other = self.parts[1]
        if "ENERGY" in energy_or_other:
            energy = self.parts[2:]

            power_all = [s for s in energy if "Power" in s]
            
            power = power_all[0]
            power_val = float(power.split(":", 1)[1])
            apparent_power = power_all[1]
            reactive_power = power_all[2]

            self.logger.debug(f"Tasmoto message --> ENERGY = {power_val} .... Aparent = {apparent_power} ... Reactive = {reactive_power}")

            db_assistant = DBTransAssistant()
            db_assistant.save_ac_power_consumption(time=measurement_time, power_consumption=power_val, topic_id=self.topic_id)
        elif "Uptime" in energy_or_other:
            uptime = energy_or_other
            self.logger.debug(f"Tasmoto message --> Uptime = {uptime}")
        elif "AM2301" in energy_or_other:
            temperature = energy_or_other
            self.logger.debug(f"Tasmoto message --> Temperature = {temperature}")
            db_assistant = DBTransAssistant()
            db_assistant.save_rack_temperature(measurement_time, temperature, topic_id=self.topic_id)
        else:
            unknown = energy_or_other
            self.logger.debug(f"Tasmoto message --> OTHER = {unknown}")

        return

    def handle(self):
        if self.parts is not None:
            # Have to handle a message from a Sonoff device
            self.logger.debug("Ready to handle a Tasmoto Message !")
            self.handle_tasmoto()

            return

        self.logger.debug("Inside the handle Function !!!")

        self.save_to_db()

    def handle_all_status(self):
        self.logger.debug(f"Ready to Save All-Sensors-Status Data !\nSize of Message Parts = {len(self.parts)}\nMessage Parts are = {self.parts}\nTopic ID = {self.topic_id}\nTime = {self.parts[0]}")

        time_full = self.parts[0]
        time_i = time_full.find(":")
        time_val = time_full[time_i+1:]
        time_val = time_val.replace("\"", "")

        self.logger.debug(f"Measurement Time Value is ... {time_val}")

        uptime_full = self.parts[1]
        uptime_i = uptime_full.find(":")
        uptime_val = uptime_full[uptime_i+1:]
        uptime_val = uptime_val.replace("\"", "")

        self.logger.debug(f"Uptime Value in Days is ... {uptime_val}")

        uptime_secs_full = self.parts[2]
        uptime_secs_i = uptime_secs_full.find(":")
        uptime_secs_val = uptime_secs_full[uptime_secs_i+1:]
        uptime_secs_val = uptime_secs_val.replace("\"", "")

        self.logger.debug(f"Uptime Value in Secs is ... {uptime_secs_val}")

        db_assistant = DBTransAssistant()
        db_assistant.save_sensors_status(self.topic_id, time_val, uptime_val, uptime_secs_val)

    def handle_temperature_humidity(self):
        #self.logger.debug(
        #     "/////////////////////////// Going to Read Room Temperature & Humidity ////////////////////////////\n"+
        #    f"///////// {self.measurement} ///////// {self.topic_id} /////////")
        msg_parser = MessageParser(self.measurement)
        [time, temperature, humidity, temperatureUnit] = msg_parser.parse_temperature_humidity_response()

        db_assistant = DBTransAssistant()
        db_assistant.save_temperature_humidity(time_value=time, topic_id=self.topic_id, 
            temperature_value=temperature, humidity_value=humidity, temperature_unit=temperatureUnit)

        return


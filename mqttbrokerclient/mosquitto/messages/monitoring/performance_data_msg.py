"""
Creates a message with correct MQTT-format containing the edge-board ...
performance data.
"""

import time

class PerformanceDataMqttMessage(object):
    """
    This class has attributes encapuslating all performace-data components
    and a custom to-string() function that is preparing a nice string that
    is easy to transfer with mqtt-broker.
    """

    def __init__(self, job_data):
        """
        Defines the class member variable with performance-data collected
        from appropriate jobs that runs every XX secs.
        """
        self.job_data = job_data

    def prepare_mqtt_msg(self):
        """
        Create from job_data a message that can be published into an
        mqtt-broker and finally forwarded to central-server.
        """
        return str(self.job_data) + "; Datetime=" + time.ctime()

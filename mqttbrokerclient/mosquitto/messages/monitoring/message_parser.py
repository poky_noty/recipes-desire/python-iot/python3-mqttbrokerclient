"""
General message parser ... Handle new mqtt messages arrived
"""
import os
import logging

class MessageParser(object):
    """
    Each new mqtt-message contains a number of important attributes ...
    as a long string with a Datetime at the end
    This class will parse it and assign values found to member variables.
    """

    def __init__(self, _message_payload):
        self._message_payload = _message_payload

        self.measurement = None
        self.measur_datetime = None

        self._logger = logging.getLogger("mqttbroker-client")
        self._logger.debug(f"Just created an MQTT-Message Parser for --> {self._message_payload} !")

        try:
            self.delimiter = os.environ["MQTT-MESSAGE-DELIMITER"]
        except Exception as ex:
            self._logger.debug("No Delimiter Found in External Configuration ... Using Default = ;\n"
              f"[exception: {str(ex)}]")
            self.delimiter = ';'

    def parse(self):
        """
        Parse the new mqtt-message and discover important information ...
        Save crucial data like "time of message-generation" to variables for
        future reference.
        """
        split_parts = self._message_payload.split(self.delimiter)

        if split_parts is None:
            # WARNING: wrong delimiter 00 cannot split message payload
            self._logger.warn(f"Cannot Split MQTT-Message {self._message_payload} with Delimiter {self.delimiter} !")
            raise Exception("Aborting Message Splitting ... Wrong Delimiter!")

        self.measurement = split_parts[0]
        self.measur_datetime = split_parts[1]

        self._logger.debug(
            f"Inside the Message Parser ... Parts={split_parts}")

        return split_parts

    def parse_temperature_humidity_response(self):
        parts = self._message_payload.split(self.delimiter)

        time_all = parts[0]
        
        temperature_all = parts[1]
        temperature_less = temperature_all[temperature_all.find("Temperature"):].replace("\"", "")
        temperature_val = temperature_less.split(":")[1].strip()

        tempUnit_all = parts[3]
        tempUnit_less = tempUnit_all[tempUnit_all.find("TempUnit"):].replace("\"", "").replace("}", "").replace("'", "")
        tempUnit_val = tempUnit_less.split(":")[1].strip()

        humidity_all = parts[2]
        humidity_less = humidity_all[humidity_all.find("Humidity"):].replace("\"", "").replace("}", "")
        humidity_val = humidity_less.split(":")[1].strip()

        time_less = time_all[time_all.find("{\"Time\":"):]
        time_less = time_less.replace("\"", "")
        time_val = time_less.split(":")[1].strip()

        self._logger.debug(f"=====> {time_val} / {temperature_val} / {humidity_val} / {tempUnit_val}")

        return [time_val, temperature_val, humidity_val, tempUnit_val]

    def get_measurement(self):
        return self.measurement

    def get_measur_datetime(self):
        return self.measur_datetime

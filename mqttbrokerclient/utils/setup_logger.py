import logging
import os

def setup_logger():
    setup_app_logger()
    setup_other_loggers()

def setup_app_logger():
    """
    Setup a global logger that keeps track of mqttbroker-client important
    events.
    """

    # Create logger for mqttbroker-client application
    logger = logging.getLogger("mqttbroker-client")

    log_level = os.environ["LOG_LEVEL"]
    logger.setLevel(log_level)

    # Create file handler which logs to specific folder/name.log
    log_filename = os.environ["LOG_FILENAME"]
    file_h = logging.FileHandler(log_filename)
    file_h.setLevel(log_level)

    # Create console handler with a higher log level
    console_h = logging.StreamHandler()
    console_h.setLevel(logging.DEBUG)

    # Create formatter and add it to the handlers
    formatter = logging.Formatter(
        "%(asctime)s - 5(name)s - %(levelname)s - %(message)s")
    file_h.setFormatter(formatter)
    console_h.setFormatter(formatter)

    # Add the handlers to the logger
    logger.addHandler(file_h)
    logger.addHandler(console_h)

    # logger is ready for application messages
    logger.info("Logger is Ready 222 ... Sending logs to Files & Console")

def setup_other_loggers():
    # Create logger for mqttbroker-client application
    logger = logging.getLogger("redis-watchdog")

    log_level = logging.DEBUG

    logger.setLevel(log_level)

    # Create file handler which logs to specific folder/name.log
    log_filename = "/var/log/redis-watchdog.log"
    file_h = logging.FileHandler(log_filename)
    file_h.setLevel(log_level)

    # Create console handler with a higher log level
    console_h = logging.StreamHandler()
    console_h.setLevel(log_level)

    # Create formatter and add it to the handlers
    formatter = logging.Formatter("%(asctime)s - 5(name)s - %(levelname)s - %(message)s")
    file_h.setFormatter(formatter)
    console_h.setFormatter(formatter)

    # Add the handlers to the logger
    logger.addHandler(file_h)
    logger.addHandler(console_h)

    # logger is ready for application messages
    logger.info("Redis-Watchdog Logger is Ready ... Sending logs to Files & Console")


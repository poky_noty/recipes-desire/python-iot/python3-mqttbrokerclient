import os

from .datastore.datastore_configuration import DatastoreConfiguration

class ExternalConfigurationShovel(object):
    """
    Achieve Software-Quality: Build Once, Run Anywhere.
    Externalize Application Configuration.
    """

    def __init__(self):
        """
        Try to read all configuration parameters ...
        Might be hundreds of environment variables used for this purpose
        """

        # Get the necessary data for connecting a clinet to MQTT-Broker
        self.broker = {
            "ip": os.environ["MOSQUITTO_HOST"],
            "port": os.environ["MOSQUITTO_PORT"],
            "keepalive": 60
        }

        self.datastore_config = DatastoreConfiguration()

    def get_broker(self):
        """
        Return all data needed for connecting to MQTT-Broker
        """
        return self.broker

    def get_datastore_configuration(self):
        return self.datastore_config

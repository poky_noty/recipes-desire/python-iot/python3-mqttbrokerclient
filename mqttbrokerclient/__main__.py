import os
import time
import traceback

import logging
from time import sleep

from .main_edge import MainEdge
from .main_server import MainServer

#from mqttbrokerclient.utils.setup_logger import setup_logger
from mqttbrokerclient.utils.setup_logger import setup_logger
#from mqttbrokerclient.datastore.db_manager import DBManager
#from mqttbrokerclient.messages.monitoring.host_discovery_msg\
#    import publish_msg_hosts_up
from mqttbrokerclient.simple_message_manager import prepare_mqtt_clients

from .connection_manager import is_datastore_available

ACCEPTABLE_APP_CONTEXT = ["SERVER-SIDE", "EDGE-BOARD"]

POLLING_INTERVAL_SECS = 5

def main():
    """ Run the application in an endless manner.
    The main function queries database every XX secs.
    Also there is a Subscriber that is waiting for interesting MQTT messages.
    """
    # Setup logger for the whole application
    try:
        setup_logger()
        logger = logging.getLogger("mqttbroker-client")
    except Exception as ex:
        print(f"Exception is Raised ...{str(ex)}")
        return

    # DEBUG SECTION
    #while True:
    #    logger.debug("Hello LG !")
        # Wait for next XX secs
    #    sleep(12)

    # What is the application context ... on Server-Side OR Edge-Board
    app_context = os.environ["APP-CONTEXT"]
    if app_context is None:
        # Not defined the app-context
        raise Exception("Undefined Application-Context ... Aborting!")
    elif app_context not in ACCEPTABLE_APP_CONTEXT:
        # Wrong app-context
        raise Exception("Invalid Application-Context ... Aborting!")

    # ****************************************
    # Where this MQTTBroker-Client is running?
    # ****************************************
    if "SERVER" in app_context.upper():
        # The Server-Side Broker-Client
        logger.debug("Module that is Handling MQTT-Messages at SERVER-side")
        #MainServer().run()
        db_watchdog = DBWatchdog(POLLING_INTERVAL_SECS)
        db_watchdog.start_monitoring()

        central_main = MainServer()
        central_main.create_scheduler()
        central_main.run_scheduler()

        # Initialize the DB-Manager and establish a database connection
        logger.debug("Leaving MAIN function ... for Datastore-Availability-Check")
        is_datastore_available()

    elif "EDGE" in app_context.upper():
        # The Edge-Side Broker-Client
        try:
            logger.debug("Module that is Handling MQTT-Messages at EDGE-side")
            my_main = MainEdge()
            my_main.create_scheduler()
            my_main.run_scheduler()
        except Exception as x:
            logger.error(f"Exception Raised ... {x}")

    logger.debug("Ready to run mqttbroker-client on ... %s", app_context)

    #return

    # ***************************************************************
    # Define Publishers and Subscribers for MQTT-Broker communication
    # ***************************************************************
    """try:
        prepare_mqtt_clients()
    except Exception as mqtt_err:
        # Aborting
        logger.error(
            "Failed Establishing Connection with MQTT-Broker ... Aborting\n[Msg]: %s",
            str(mqtt_err))

        traceback.print_exc()

        #return
    """
    
    #return
    #db_manager = DBManager()

    #try:
    #    db_manager.connect()
    #except Exception as conn_err:
        # Aborting
    #    logger.error(
    #        "Failed Establishing Connection with Database ... Aborting\n[Msg]: %s",
    #        str(conn_err))

        #return

    #while True:
        # start scaning for new records
    #    new_servers_for_aliveness = db_manager.check_table_for_new_records(
    #        "PingTargetServers")
    #    new_servers_mqtt_msg = publish_msg_hosts_up(
    #        new_servers_for_aliveness)
    #    logger.debug("Ready to Poll Database for New Monitoring-System Administration Data !")
    #    time.sleep(15)

"""
    **************************************
    **** Application Main Entry Point ****
    **************************************
"""
main()

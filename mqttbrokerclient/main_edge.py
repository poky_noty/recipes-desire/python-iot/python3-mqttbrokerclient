"""
Main module for application running at edge/sensor board
"""

import logging

from mqttbrokerclient.jobs.edge_board.eb_scheduler import EBJobScheduler
#from jobs.edge_board.eb_scheduler import EBJobScheduler

from .datastore.db_watchdog import DBWatchdog, RedisWatchdog

class MainEdge(object):
    """
    The scheduler for running the necessary tasks  at edge-board side.
    """

    def __init__(self):
        self._scheduler = None

        self._logger = logging.getLogger("mqttbroker-client")
        self._logger.debug("Bio Farma!")

        redis_watchdog = RedisWatchdog()
        redis_watchdog.try_to_connect()
        redis_watchdog.subscribe_to_channel_and_listen()
        
    def create_scheduler(self):
        """
        Create SCHEDULER to add tasks && run at predefined time-intervals
        """
        self._scheduler = EBJobScheduler()

    def run_scheduler(self):
        """
        Instruct scheduler to start running tasks.
        """
        self._scheduler.schedule_all_jobs()

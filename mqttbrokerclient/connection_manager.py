from .simple_message_subscriber import quick_subscribe_test
from .external_configuration_shovel import ExternalConfigurationShovel

from .datastore.db_manager import DBManager

import logging
logger = logging.getLogger("mqttbroker-client")

def is_broker_available(curiosity_rover, topic):
    '''
    Check the Connection and Verify that the MQTT-Broker is Up & Listening
    '''
    #logger = curiosity_rover.get_logger()
    #logger.debug("Ready to Check Local-Data-Collector containerA connection with .. "+
    #             "Eclipse-Mosquitto Broker containerB")

    #broker = curiosity_rover.get_broker()
    #simple_subscriber = SimpleSubscriber(logger, broker, topic)
    quick_subscribe_test()


def is_datastore_available():
    """
    Check whether the repository for storing and sharing measured data is
    available.
    """
    logger.debug(
        "Ready to Check Validity of Datastore External Configuration Data")

    ext_config_shovel = ExternalConfigurationShovel()
    ext_config_datastore = ext_config_shovel.get_datastore_configuration()
    ext_config_datastore.evaluate()

    logger.debug(
        "Is Backend of type Database .... %s",
        str(ext_config_datastore.is_database()))

    logger.debug(
        "Is Backend of type MessageQueu .... %s",
        str(ext_config_datastore.is_message_queue()))

    logger.debug(
        "Is MySQL available for Permanent Storage .... %s",
        str(ext_config_datastore.is_mysql()))

    logger.debug(
        "Is Redis available for Message Exchange .... %s",
        str(ext_config_datastore.is_redis()))

    db_manager = DBManager()
    db_manager.connect_or_raise_exception()

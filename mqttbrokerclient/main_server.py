"""
Module with code for running the application on server-side as a ...
data agrregator. The data is stored on database and examined for ...
performance anomalies.
"""
import logging

#from mqttbrokerclient.jobs.central_server.cs_scheduler import CSJobScheduler
from .jobs.central_server.cs_scheduler import CSJobScheduler
from mqttbrokerclient.mosquitto.client_subscriber import nice_main

class MainServer(object):
    """
    The scheduler for running the necessary tasks at CENTRAL-SERVER side.
    """

    def __init__(self):
        self._scheduler = None

        self._logger = logging.getLogger("mqttbroker-client")
        self._logger.debug(
            "Inisde the MainFunction() for "
            "SERVER-SIDE Application Context !")

    def create_scheduler(self):
        """
        Create SCHEDULER to add some important tasks &&
        run them at predefined time-intervals.
        """
        self._scheduler = CSJobScheduler()

    def run_scheduler(self):
        """
        Instruct scheduler to start running tasks.
        """
        self._scheduler.schedule_all_jobs()

        #nice_main()

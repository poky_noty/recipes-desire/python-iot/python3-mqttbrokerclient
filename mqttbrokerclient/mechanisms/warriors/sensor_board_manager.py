"""
A module for CPU, RAM, DISK, NETwork monitoring -- ARM boards
"""


class SensorBoardManager(object):
    """
    General manager that is responsible for calling other more specific ...
    managers, ie Is-Alive manager.
    """

    def __init__(self, board_id):
        self.board_id = board_id
        self._is_alive = False
        self._is_cpu_ok = False

    def is_board_healthy(self):
        """
        Check whether a sensor-board is healthy - apply a number of tests
        """
        alive_manager = SensorBoardAliveManager(self.board_id)
        self._is_alive = alive_manager.check()

        cpu_manager = SensorBoardCPUManager(self.board_id)
        self._is_cpu_ok = cpu_manager.check()

    @property
    def is_alive(self):
        return self._is_alive

class SensorBoardCPUManager(object):
    def __init__(self, board_id):
        self.board_id = board_id

    def check(self):
        return False

class SensorBoardAliveManager(object):
    """
    Check whether the board is constantly sending `I-am-alive` signals.
    Apply a formula to show the number of msecs passed since ...
    last-contact-time and check that the time difference is smaller a ...
    threshold value.
    """

    def __init__(self, id):
        self.board_id = id

    def check(self):
        return False

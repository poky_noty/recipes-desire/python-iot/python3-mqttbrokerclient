from abc import ABC, abstractmethod

import logging
#import dmidecode


class AbstractCollector(ABC):
    """
    Template for all classes that collect various kind of performance data.
    """

    def __init__(self):
        """
        Initialize basic attributes ...
        The name/id of monitored board and type of performance data.
        """
        self._logger = logging.getLogger("mqttbroker-client")

        self._board_id = None
        self.collect_board_id()

        self._performance_data = None

        super().__init__()

        self.collect_performance_data()

    @abstractmethod
    def collect_performance_data(self):
        """
        Collects all table records ... no filtering applied
        """
        pass

    def collect_board_id(self):
        """
        Use DMIDecode to get hardware information about system baseboard.
        """
        #self.board_id = dmidecode.baseboard()
        pass

    def get_performance_data(self):
        """
        Get latest collected performance data
        """
        return self._performance_data

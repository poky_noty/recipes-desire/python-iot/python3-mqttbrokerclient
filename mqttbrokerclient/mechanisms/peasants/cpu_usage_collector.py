"""
A module for retrieving information on running processses and ...
system utilization (CPU, Memory, Hard Disks, Networks).
Also collects data from sensors.

Very Important --- Code running on edge/sensor-board.
"""
import psutil

from .cpu_usage_data import CPUsageData


class CPUsageCollector(object):
    """
    This class is responsible for data collection on specific edge board ...
    Specifically provides the 'Current CPU-Usage'
    """

    def __init__(self, board_id):
        self.board_id = board_id
        self.sys_info = None

    def get_board_id(self):
        return -1

    def get_sys_info(self):
        #return CPUsageData(
        #    board-id=self.get_board_id(),
        #    "performance-data": get_performance_data()
        #)
        return -1

"""
This module makes easier the communication with the mqtt-broker for
publishing and receiving any mqtt-messages.
"""

import os 

from mqttbrokerclient.mosquitto.client_publisher import ClientPublisher

from mqttbrokerclient.mosquitto.messages.monitoring.performance_data_msg \
    import PerformanceDataMqttMessage

from mqttbrokerclient.mosquitto.messages.monitoring.message_parser \
    import MessageParser

from mqttbrokerclient.mosquitto.messages.monitoring.message_handler \
    import MessageHandler

def send_message_about_board_performance(data_collected):
    """
    Communicate latest board-performance measurements with a central module ...
    Prepare the appropriate MQTT-message and publish it to appropriate topic at
    broker.
    """
    msg = ""

    """
    ****************************************************
    ****************************************************
    Publish Collected Data --> Send to Local MQTT-Broker
    ****************************************************
    ****************************************************
    """

    simple_publisher = ClientPublisher(is_sensordata=True)
    simple_publisher.connect_to_broker()
    simple_publisher.publish_and_wait(
        PerformanceDataMqttMessage(data_collected).prepare_mqtt_msg())
    #sss
    #simple_publisher.publish_and_wait()

def parse_new_message_and_find_handler(msg_topic, todo_new_msg):
    """
    A new message arrived at broker ... need to find the meaning of the message
    and assign the appropriate handler
    """
    simple_parser = MessageParser(todo_new_msg)
    parts = simple_parser.parse()

    topic = os.environ["MQTT-DATA-TOPIC"]

    if "tele" in topic:
        # This is a message from Tasmoto
        msg_handler = MessageHandler(parts=parts, topic_id=msg_topic)
        msg_handler.handle()

        return

    measurement = simple_parser.get_measurement()
    the_time = simple_parser.get_measur_datetime()

    simple_handler = MessageHandler(measurement=measurement, the_time=the_time)
    simple_handler.handle()

def save_all_sensors_status_table(msg_topic, msg):
    
    simple_parser = MessageParser(msg)
    parts = simple_parser.parse()

    topic = os.environ["MQTT-DATA-TOPIC"]

    if "tele" in topic:
        # This is a message from Tasmoto
        msg_handler = MessageHandler(parts=parts, topic_id=msg_topic)
        msg_handler.handle_all_status()

        return

    return

def handle_sensor_status_response(msg_topic, msg):
    # Is it a status message from a temp-hum sensor ?

    if "lamp1/STATUS10" in msg_topic:
        # Important STATUS message with how hot/ cold and wet/ dry environment 
        simple_handler = MessageHandler(topic_id=msg_topic, measurement=msg)
        simple_handler.handle_temperature_humidity()

import os
import logging

from mqttbrokerclient.datastore.db_manager import DBManager

from mqttbrokerclient.datastore.influxdb.connect_to_local import get_connection
from mqttbrokerclient.datastore.influxdb.data_point import create_json_body_for_data_point

class DBTransAssistant(object):
    def __init__(self):
        self.db_manager = DBManager()
        self.db_manager.connect_or_raise_exception()

        self.use_influxdb = bool(os.environ["USE-INFLUXDB"])

        self.logger = logging.getLogger("mqttbroker-client")
        self.logger.debug("Inside the DB-Transactions-Assistant")

    def save_system_stats(self, measurement, measur_datetime):
        self.logger.debug("New Data Available from ... System-Resources-Used")

        self.db_manager.save_system_stats(measurement, measur_datetime)

    def save_rack_temperature(self, time, temperature, topic_id):
        """
        Saves the measured temperature in Rack to database
        """
        self.logger.debug("Temperature Management in Data Centers - Store to Database Measurement:\n"
            f"Time Taken : {time}\n"
            f"Temperature: {temperature}")

        # Remove the Headers from response strings:
        # Time Taken : b'{"Time":"2019-12-23T10:41:09"
        # Temperature: "AM2301":{"Temperature":22.8
        time_value = time.split("\"Time\":")[1]
        temperature_value = temperature.split("\"Temperature\":")[1]
        self.logger.debug(f"Ready to store --> Time = {time_value}  &&&&&& Temperature = {temperature_value}") 

        self.db_manager.save_ac_hot_air_temperature( [time_value, temperature_value] )

        self.save_to_influxdb(measurement_type="temperature", time_value=time_value, topic_id=topic_id, measurement=temperature_value)

    def save_ac_power_consumption(self, time, power_consumption, topic_id):
        """
        Save record {time, power_consumption} to the database
        """
        time_value = time.split("\"Time\":")[1]

        self.db_manager.save_ac_power_consumption([time_value, power_consumption])

        self.save_to_influxdb(measurement_type="power", time_value=time_value, topic_id=topic_id, measurement=power_consumption)

        return

    def save_sensors_status(self, topic_id, time_value, uptime, uptime_secs):
        self.logger.debug("I am going to save Sensors Status to InfluxDB !")

        if "SENSOR" in topic_id:
            self.logger.debug("This is a SENSOR mqtt-message ... No STATUS data contained")
            return

        measurement_type = "sensors_status"
        
        measurement = uptime
        measurement_secs = uptime_secs

        self.save_to_influxdb(measurement_type=measurement_type, time_value=time_value, topic_id=topic_id, measurement=measurement_secs)

        return

    def save_temperature_humidity(self, topic_id, time_value, temperature_value, humidity_value, temperature_unit):
        self.logger.debug("Ready to store to InfluxDB the Temperature & Humidity values !")

        self.save_to_influxdb(
            measurement_type="temperature-humidity", topic_id=topic_id, time_value=time_value, 
            measurement={
                "temperature": temperature_value, 
                "humidity": humidity_value, 
                "temperature-unit": temperature_unit,
            }
        )

    def save_to_influxdb(self, measurement_type, time_value, topic_id, measurement, host="edgeboard01", region="vlab3"):
        if self.use_influxdb:
            self.logger.debug("I am going to save data to mysql + INFLUXDB !") 

            connection = get_connection()
            #self.logger.debug(f"The InfluxDB located at: {connection}")

            time_value = time_value.replace('"', '')
            json_body = create_json_body_for_data_point(measurement=measurement_type, time=time_value, topic_id=topic_id, host=host, region=region, value=measurement)
            self.logger.debug(f"json-body: {json_body}")

            # SOS: Measurement data stored to influxDB
            status = connection.write_points(json_body)
            if status:
                self.logger.debug("Successful addition into InfluxDB ... New Data Point")
            else:
                self.logger.warn("Failed to Add Data Point into InfluxBD !")

        return